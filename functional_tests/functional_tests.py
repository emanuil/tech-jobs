from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import WebDriverException
from django.test import LiveServerTestCase
# Describe a user story and write tests for each step in the user flow


class NewVisitorTest(LiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def test_home_page_displays_correctly(self):
        self.browser.get('http://localhost:8000/')
        home_message = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Tech Jobs', home_message)

    def test_jobs_page_displays_job_posts(self):
        # A user visits the home page and can view job posts
        self.browser.get('http://localhost:8000/jobs')
        self.assertIn('Job Posts', self.browser.title)

        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Job List', header_text)
        # Test incomplete


class ApplicantSignupTest(LiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def test_signup_page_displays_correct_form(self):
        pass

    def test_signup_new_applicant(self):
        pass
        '''
        self.browser.get('http://localhost:8000/signup/applicant')

        self.find_element_by_id('')'''