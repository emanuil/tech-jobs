from django.contrib.sitemaps import Sitemap
from django.urls import reverse

from content.models import BlogPost
from .models import Company, JobPost


class PostSitemap(Sitemap):
    changefreq = "weekly"
    priority = 0.9

    def items(self):
        return BlogPost.objects.all()

    def lastmod(self, obj):
        return obj.created


class CompanySitemap(Sitemap):
    priority = 0.8

    def items(self):
        return Company.objects.all()

    def lastmod(self, obj):
        return obj.modified_date


class JobPostSitemap(Sitemap):
    changefreq = 'daily'
    priority = 0.9

    def items(self):
        return JobPost.objects.filter(expired=False)

    def lastmod(self, obj):
        return obj.publish_date


class StaticViewSitemap(Sitemap):
    priority = 1.0

    def items(self):
        return ['home', 'help', 'prices', 'registration', 'landing-salary-check']

    def location(self, obj):
        return reverse(obj)


sitemaps = {
    'static': StaticViewSitemap,
    'posts': PostSitemap,
    'jobs': JobPostSitemap,
    'companies': CompanySitemap,
}