import csv
import os
import datetime

from django.core.management.base import BaseCommand, CommandError

from jobboard.models import Salary

class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        help = 'Import Salary Data'

        with open('salaries.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                date = datetime.datetime.strptime(row[0], '%m/%d/%Y %H:%M:%S').date()
                salary = int(row[1])
                years = int(float(row[2]))
                role = row[3]
                location = row[4]

                if years < 1:
                    experience = '0-1'
                elif years >= 1 and years <= 3:
                    experience = '1-3'
                elif years > 3 and years <= 6:
                    experience = '4-6'
                elif years > 5 and years <= 10:
                    experience = '7-10'
                else:
                    experience = '10+'

                if location not in {'Sofia', 'Plovdiv', 'Varna', 'Burgas', 'Ruse', 'Stara Zagora', 'Remote', 'Work Abroad'}:
                    location = 'Other'

                sal = Salary(role=role, experience=experience, location=location, salary=salary, date=date)
                sal.save()
