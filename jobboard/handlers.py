from storages.backends.s3boto3 import S3Boto3Storage
from django.conf import settings


class CustomBoto3Storage(S3Boto3Storage):
    def __init__(self, *args, **kwargs):
        kwargs['bucket']  = settings.AWS_FILES_BUCKET_NAME
        self.location = '/content/'
        return super().__init__(*args, **kwargs)

    def _save_content(self, obj, content, parameters):
        new_parameters = parameters.copy() if parameters else {}
        if new_parameters.get('ContentType') in ['application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'application/pdf', 'text/plain']:
            new_parameters['ContentDisposition'] = 'attachment'
        return super()._save_content(obj, content, new_parameters)