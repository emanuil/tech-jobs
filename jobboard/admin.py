from django.contrib import admin
from django.template.loader import render_to_string
from django.core.mail import send_mail, send_mass_mail, EmailMessage, EmailMultiAlternatives
from jobboard.models import Applicant, JobPost, Application, Company, Purchase, Subscriber, Salary, Lead, Offer


class ApplicantAdmin(admin.ModelAdmin):
    readonly_fields = ('last_query',)
    search_fields = ['full_name', 'role']
    list_filter = ('role', 'experience', 'location')
    list_display = ['full_name', 'role', 'experience', 'current_salary', 'skills_list', 'location', 'resume', 'email_address']
    list_select_related = ['user']

    actions = ['send_activation_email', 'make_active', 'make_inactive', 'send_talentmarketplace_email']

    def send_activation_email(self, request, queryset):
        mails = []
        for applicant in queryset:
            subject = 'Companies want to apply to you!'
            message = '''
Hello {},

We would like to invite you to join our new Talent Marketplace, where job opportunities come to you.

By completing your Tech Hire profile you can start receiving direct job offers with salaries and benefits from multiple companies. You can review offers and share your contact details only with the companies you like.

Good IT specialists are in great demand and even if you are not actively looking it is a good idea to stay open to new opportunities. At Tech Hire our goal is to make sure you do not miss the amazing job that you have always wanted.

In order for companies to be able to send you offers, you must complete your profile with current role and relevant skills https://techhire.bg/applicant/{}/update.

Regards,
Tech Hire'''.format(applicant.full_name.split()[0], applicant.id)
            mail = (subject, message, 'emanuil@techhire.bg', [applicant.user.email])
            mails.append(mail)

        send_mass_mail(tuple(mails), fail_silently=False)

    def send_talentmarketplace_email(self, request, queryset):
        for applicant in queryset:
            subject = 'Companies can now apply to you!'
            context = {'name': applicant.full_name.split()[0], 'applicant': applicant}
            text_content = render_to_string('email/talent_search.txt', context)
            html_content = render_to_string('email/talent_search.html', context)
            mail = applicant.user.email
            msg = EmailMultiAlternatives(subject, text_content, 'emanuil@techhire.bg', [mail])
            msg.attach_alternative(html_content, 'text/html')
            msg.send()
        return

    def make_active(self, request, queryset):
        for applicant in queryset:
            applicant.looking = True
            applicant.save()

    def make_inactive(self, request, queryset):
        for applicant in queryset:
            applicant.looking = False
            applicant.save()

    def get_queryset(self, request):
        return super(ApplicantAdmin, self).get_queryset(request).prefetch_related('skills')

    def skills_list(self, obj):
        return u', '.join(o.name for o in obj.skills.all())

    def email_address(self, obj):
        return obj.user.email


class CompanyAdmin(admin.ModelAdmin):
    readonly_fields = ('slug', )
    list_display = ['name', 'company_email' , 'credits', 'last_job_post_date']


    def company_email(self, obj):
        return obj.user.email


class JobPostAdmin(admin.ModelAdmin):
    readonly_fields = ('slug', 'created_date')
    search_fields = ['title', 'company__name',]
    list_filter = ('role', 'experience', 'location')
    list_display = ['title', 'company_name', 'role', 'experience', 'job_type', 'skills_list', 'min_salary', 'max_salary', 'currency', 'location', 'applications', 'publish_date', 'expired']
    list_select_related = ('company',)
    ordering = ['-publish_date']

    def company_name(self, obj):
        return obj.company.name

    def get_queryset(self, request):
        return super(JobPostAdmin, self).get_queryset(request).prefetch_related('skills', 'applications')

    def skills_list(self, obj):
        return u', '.join(o.name for o in obj.skills.all())

    def applications(self, obj):
        return obj.applications.count()


class ApplicationAdmin(admin.ModelAdmin):
    readonly_fields = ('created_date',)
    list_display = ['job_title', 'applicant_name', 'resume', 'job_role', 'company', 'created_date']
    list_select_related = ['job_post', 'applicant']
    search_fields = ['job_title', 'company']

    def job_title(self, obj):
        return obj.job_post.title

    def applicant_name(self, obj):
        return obj.applicant.full_name

    def job_role(self, obj):
        return obj.job_post.role

    def company(self, obj):
        return obj.job_post.company.name


class PurchaseAdmin(admin.ModelAdmin):
    readonly_fields = ('date',)
    list_display = ['company', 'amount', 'currency', 'credits', 'invoice', 'completed', 'date']
    list_filter = ['completed']


class SubscriberAdmin(admin.ModelAdmin):
    readonly_fields = ('created_date',)


class SalaryAdmin(admin.ModelAdmin):
    list_display = ['role', 'experience', 'location', 'salary', 'date']
    list_filter = ['role', 'experience', 'location']
    search_fields = ['salary']

class LeadAdmin(admin.ModelAdmin):
    list_display = ['email','role', 'experience', 'date']
    list_filter = ['role', 'experience',]
    search_fields = ['email']


class OfferAdmin(admin.ModelAdmin):
    list_display = ['applicant', 'company', 'title', 'min_salary', 'max_salary', 'active', 'interested']
    list_filter = ['company', 'role', 'interested']
    search_fields = ['title']


# Register your models here.
admin.site.register(Applicant, ApplicantAdmin)
admin.site.register(Company, CompanyAdmin)
admin.site.register(JobPost, JobPostAdmin)
admin.site.register(Application, ApplicationAdmin)
admin.site.register(Purchase, PurchaseAdmin)
admin.site.register(Subscriber, SubscriberAdmin)
admin.site.register(Salary, SalaryAdmin)
admin.site.register(Lead, LeadAdmin)
admin.site.register(Offer, OfferAdmin)