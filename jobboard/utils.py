from .models import JobPost, Applicant

def calculate_price(credits):
    if credits < 10:
        price = credits * 50
    elif credits < 25:
        price = credits * 45
    elif credits < 50:
        price = credits * 42.5
    elif credits < 100:
        price = credits * 40
    else:
        price = credits * 37.5

    return price


def similar_jobs(job):
    return [similar for similar in job.skills.similar_objects() if similar.model_name() == 'job post' and similar.similar_tags > 1]
