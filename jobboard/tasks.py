from celery import shared_task
from celery.decorators import task, periodic_task
from django.core.mail import send_mail, send_mass_mail, EmailMultiAlternatives
from django.template.loader import render_to_string
from django.core import serializers
from .models import Applicant, Company, JobPost, Application
import datetime


@task
def save_query_task(applicant_id, search):
    applicant = Applicant.objects.get(id=applicant_id)
    queryset = JobPost.objects.search(q=search)
    if queryset.exists():
        applicant.last_query = search
        applicant.last_query_date = datetime.date.today()
        applicant.save()
        return 'Saved'


@task
def mark_expired_jobs():
    jobs = JobPost.objects.filter(expired=False).select_related('company')

    for job in jobs:
        if job.expiry_date <= datetime.date.today():
            job.expired = True
            job.save()
            applications = Application.objects.search(job_post=job)
            interviews = applications.filter(status='Interview').exclude(interview_date__isnull=True).count()
            offers = applications.filter(status='Offer').count()
            hired = applications.filter(status='Hired').count()
            subject = 'Your job listing for {} has expired.'.format(job.title)
            link = '{}/job/{}/'.format(job.company.slug, job.pk)
            context = {'job': job, 'company': job.company, 'applicants': applications.count(), 'link':link, 'interviews': interviews, 'offers': offers, 'hired': hired}
            text_content = render_to_string('email/job_expired_mail.txt', context)
            html_content = render_to_string('email/job_expired_mail.html', context)
            msg = EmailMultiAlternatives(subject, text_content, 'mail@techhire.bg', [job.company.user.email])
            msg.attach_alternative(html_content,'text/html')
            msg.send()

