from django.test import TestCase
from jobboard.models import Applicant, JobPost, Application, Company, Purchase, Subscriber
from django.contrib.auth.models import User
from django.db import IntegrityError
import datetime
import pytest
# Create your tests here.


@pytest.fixture
@pytest.mark.django_db
def get_user():
    user = User.objects.create_user('first',
                                    first_name='New',
                                    last_name='User',
                                    email='first@testmail.com',
                                    password='123456')
    return user


@pytest.fixture
@pytest.mark.django_db
def get_applicant():
    applicant = Applicant()
    applicant.user = User.objects.first()
    applicant.phone = '08525324'
    applicant.location = 'Sofia'
    applicant.looking = True
    applicant.roles = []
    applicant.skills = []
    applicant.degree = 'Bachelors'
    applicant.last_application_date = datetime.date.today()
    applicant.resume_upload_date = datetime.date.today()
    applicant.save()
    return applicant


@pytest.fixture
@pytest.mark.django_db
def get_company():
    company = Company()
    company.user = User.objects.first()
    company.name = 'Test Company'
    company.slug = 'test-company'
    company.phone = '032423423'
    company.website = 'www.testco.com'
    company.description = 'Innovative testing company.'
    company.location = 'Sofia'
    company.credits = 1
    company.last_job_post_date = datetime.date.today()
    company.save()
    return company


@pytest.fixture
@pytest.mark.django_db
def get_jobpost():
    company = Company()
    company.user = User.objects.first()
    company.name = 'Test Company'
    company.slug = 'test-company'
    company.phone = '032423423'
    company.website = 'www.testco.com'
    company.description = 'Innovative testing company.'
    company.location = 'Sofia'
    company.credits = 1
    company.last_job_post_date = datetime.date.today()
    company.save()

    company = Company.objects.first()

    jobpost = JobPost()
    jobpost.title = 'Senior Python Developer'
    jobpost.slug = 'senior-python-developer'
    jobpost.featured = True
    jobpost.company = company
    jobpost.job_type = 'Full-time'
    jobpost.min_salary = 3000
    jobpost.max_salary = 4500
    jobpost.currency = 'BGN'
    jobpost.role = 'Software Developer'
    jobpost.location = 'Sofia'
    jobpost.experience = 'Senior'
    jobpost.description = 'We are looking to hire an experienced Python developer!'
    jobpost.skills = ['Python', 'AWS', 'Git']
    jobpost.publish_date = datetime.date.today()
    jobpost.expiry_date = datetime.date.today()
    jobpost.expired = False
    jobpost.boosted = False
    jobpost.save()
    return jobpost


@pytest.mark.django_db
@pytest.mark.usefixtures('get_user')
class TestApplicantModel():

    def test_creating_and_retrieving_applicant(self):
        applicant = Applicant()
        applicant.user = User.objects.first()
        applicant.phone = '08525324'
        applicant.location = 'Sofia'
        applicant.looking = True
        applicant.roles = ['Software Developer']
        applicant.skills = ['Python', 'Java']
        applicant.degree = 'Bachelors'
        applicant.website = 'www.google.com'
        applicant.linkedin = 'www.linkedin.com'
        applicant.last_query_date = datetime.date.today()
        applicant.last_application_date = datetime.date.today()
        applicant.resume_upload_date = datetime.date.today()
        applicant.save()

        saved = Applicant.objects.all()
        assert saved.count() == 1

        saved_app = saved[0]

        assert saved_app.user == User.objects.first()
        assert saved_app.phone == '08525324'
        assert saved_app.location == 'Sofia'
        assert saved_app.looking
        assert saved_app.degree == 'Bachelors'
        assert saved_app.roles == ['Software Developer']
        assert saved_app.skills == ['Python', 'Java']
        assert saved_app.website == 'www.google.com'
        assert saved_app.linkedin == 'www.linkedin.com'
        assert saved_app.last_query_date == datetime.date.today()
        assert saved_app.resume_upload_date == datetime.date.today()
        assert saved_app.last_application_date == datetime.date.today()

    def test_cannot_save_empty_applicant(self):
        pass

    def test_cannot_save_applicant_without_cv(self):
        pass


@pytest.mark.django_db
@pytest.mark.usefixtures('get_user')
class TestCompanyModel():

    def test_create_and_retrieve_company(self):
        company = Company()
        company.user = User.objects.first()
        company.name = 'Test Company'
        company.slug = 'test-company'
        company.phone = '083242332'
        company.website = 'www.testco.com'
        company.summary = 'Test company'
        company.description = 'Innovative testing company.'
        company.location = 'Sofia'
        company.address = 'bul.Vitosha'
        company.team_size = '11-50'
        company.credits = 3
        company.last_job_post_date = datetime.date.today()
        company.save()

        saved = Company.objects.all()

        assert saved.count() == 1

        com = saved[0]

        assert com.user == User.objects.first()
        assert com.name == 'Test Company'
        assert com.slug == 'test-company'
        assert com.phone == '083242332'
        assert com.website == 'www.testco.com'
        assert com.description == 'Innovative testing company.'
        assert com.location == 'Sofia'
        assert com.address == 'bul.Vitosha'
        assert com.team_size == '11-50'
        assert com.credits == 3
        assert com.last_job_post_date == datetime.date.today()

    def test_cannot_save_empty_company(self):
        company = Company()
        with pytest.raises(IntegrityError):
            company.save() 

        saved = Company.objects.all()

        assert len(saved) == 0

    def test_cannot_have_negative_credits(self):
        pass


@pytest.mark.django_db
@pytest.mark.usefixtures('get_user', 'get_company')
class TestJobPostModel():

    def test_create_and_retrieve_jobpost(self):
        company = Company.objects.first()

        jobpost = JobPost()
        jobpost.title = 'Senior Python Developer'
        jobpost.slug = 'senior-python-developer'
        jobpost.featured = True
        jobpost.company = company
        jobpost.job_type = 'Full-time'
        jobpost.min_salary = 3000
        jobpost.max_salary = 4500
        jobpost.currency = 'BGN'
        jobpost.role = 'Software Developer'
        jobpost.location = 'Sofia'
        jobpost.experience = 'Senior'
        jobpost.description = 'We are looking to hire experienced Python developer'
        jobpost.skills = ['Python', 'AWS', 'Git']
        jobpost.publish_date = datetime.date.today()
        jobpost.expiry_date = datetime.date.today() + datetime.timedelta(days=30)
        jobpost.boosted = False
        jobpost.expired = False
        jobpost.save()

        posts = JobPost.objects.all()

        assert posts.count() == 1

        post = posts[0]

        assert post.title == 'Senior Python Developer'
        assert post.slug == 'senior-python-developer'
        assert post.featured
        assert post.company == company
        assert post.job_type == 'Full-time'
        assert post.min_salary == 3000
        assert post.max_salary == 4500
        assert post.currency == 'BGN'
        assert post.role == 'Software Developer'
        assert post.location == 'Sofia'
        assert post.experience == 'Senior'
        assert post.description == 'We are looking to hire experienced Python developer'
        assert post.skills == ['Python', 'AWS', 'Git']
        assert post.created_date == datetime.date.today()
        assert post.publish_date == datetime.date.today()
        assert post.expiry_date == datetime.date.today() + datetime.timedelta(days=30)
        assert not post.boosted
        assert not post.expired


@pytest.mark.django_db
@pytest.mark.usefixtures('get_user', 'get_applicant', 'get_jobpost')
class TestApplicationsModel():

    def test_create_and_retrieve_application(self):
        application = Application()
        application.applicant = Applicant.objects.first()
        application.job_post = JobPost.objects.first()
        application.status = 'Applied'
        application.interview_date = datetime.date.today()
        application.applicant_rating = 'Qualified'
        application.comment = 'Good candidate'
        application.save()

        apps = Application.objects.all()

        assert apps.count() == 1

        app = apps[0]

        assert app.applicant == Applicant.objects.first()
        assert app.job_post == JobPost.objects.first()
        assert app.created_date == datetime.date.today()
        assert app.status == 'Applied'
        assert app.interview_date == datetime.date.today()
        assert app.applicant_rating == 'Qualified'
        assert app.comment == 'Good candidate'


@pytest.mark.django_db
@pytest.mark.usefixtures('get_user', 'get_company')
class TestSubscriberModel():

    def test_create_and_retrieve_subscriber(self):
        subscriber = Subscriber()
        subscriber.company = Company.objects.first()
        subscriber.email = 'test@mail.com'  
        subscriber.save()

        subscribers = Subscriber.objects.all()

        assert subscribers.count() == 1

        s = subscribers[0]

        assert s.company == Company.objects.first()
        assert s.email == 'test@mail.com'
        assert s.created_date == datetime.date.today()


@pytest.mark.django_db
@pytest.mark.usefixtures('get_user', 'get_company')
class TestPurchaseModel():

    def test_create_and_retrieve_purchase(self):
        purchase = Purchase()
        purchase.company = Company.objects.first()
        purchase.amount = 100
        purchase.currency = 'BGN'
        purchase.credits = 2
        purchase.save()

        purchases = Purchase.objects.all()

        assert purchases.count() == 1

        p = purchases[0]

        assert p.company == Company.objects.first().name
        assert p.amount == '100'
        assert p.currency == 'BGN'
        assert not p.completed
        assert p.credits == 2
        assert p.date == datetime.date.today()
