"use strict";

$(function () {

var done = false;

$(window).scroll(function () {
  if ($(window).scrollTop() > $('body').height() / 2 && done==false) {
    $('#subscribeModal').modal();
    done = true;
    }
});
});