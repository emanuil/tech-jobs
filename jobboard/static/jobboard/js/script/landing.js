'use strict';

$(function () {

    $('.grid').masonry({
        itemSelector: '.grid-item',
        columnWidth: 300,
        fitWidth: true,
        gutter: 10,
    });
});