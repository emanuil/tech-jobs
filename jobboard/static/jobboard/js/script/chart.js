"use strict";


var ctx = document.getElementById('salaryChart');

console.log(salaryData);

var myLineChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['<1000 BGN', '1000 BGN  - 2000 BGN', '2000 BGN - 3000 BGN', '3000 BGN - 4000 BGN', '4000 BGN - 5000 BGN', '>5000 BGN'],
        datasets: [
        {
            label: 'Salaries',
            data: salaryData,
            backgroundColor: 'rgba(17,175,162,1)',
        }
        ]
    },
    options: {
        scales: {
            yAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Number of People',
                },
                ticks: {
                    beginAtZero:true,
                    suggestedMin: 0,
                    suggestedMax: 10,
                    stepSize: 1,
                }
            }]
        }
    }
});