"use strict";

$(function () {

   // Company Applications Page

   $('#id_interview_date').datepicker({
        autoclose: true,
        format: 'dd.mm.yyyy',
        weekStart: 1,
        startDate: '0d',
    });

    var options = {
        valueNames: ['name', 'application_date', 'status', 'rating', 'interview_date']
    };

    var appList = new List('appList', options);

    $('#search-field').on('keyup', function() {
      var searchString = $(this).val();
      appList.search(searchString);
    });

    $('.open-app').on("click", function(e){
        var columnValues = $(this).parent().siblings().map(function() {
                 return $(this).text();
        }).get();
        var cv = $(this).data("cv");
        var id = $(this).data("app-id");
        $('#saveApplication').attr("data-app-id", id);
        $('#id_name').text(columnValues[0]);
        $('#id_email').text(columnValues[1]);
        $('#id_phone').text(columnValues[2]);
        $('#id_cv').attr("href", cv);
        $('#id_application_date').text(columnValues[4]);
        $('#id_status').val(columnValues[5]);
        $('#id_applicant_rating option').filter((i,e)=>{return $(e).text() == columnValues[6]}).prop('selected', true);
        $('#id_interview_date').val(columnValues[8]);
        $('#id_comment').val(columnValues[7]);
    });

    $('#saveApplication').on("click", (e)=>{
        var pk = $('#saveApplication').attr("data-app-id");
        $.ajax({
            type: "POST",
            url: "application/" + pk + '/update/',
            data: $('#edit_application_form').serialize(),
            success: (data) => {
                $('#application'+data.id).children('.cell[data-title=Status]').text(data.status)
                $('#application'+data.id).children('.cell[data-title=Rating]').text(data.rating)
                $('#application'+data.id).children('.cell[data-title=Comment]').text(data.comment)
                $('#application'+data.id).children('.cell[data-title=Interview_Date]').text(data.interview)
                $('#applicantModal').modal('hide');

            },
            error: (data)=> {
                $('#applicantModal').modal('hide');
            }
        });
    });
});
