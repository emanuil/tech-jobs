"use strict";

$(function () {

    $('#id_role').select2({
        placeholder: '',
        containerCssClass: 'form-control'
    });
    $('#id_skills').select2({
        tags: true,
        ajax: {
            url: '/api/skills/',
            dataType: 'json',
            processResults: (data)=>{
            var text = $.map(data.results, (obj)=>{
                obj.id = obj.text;
                return obj;
            });
            return {
                results: text
            }
        }
        },

    });
    $('.make-offer').on("click", function(e){
        var id = $(this).data("talent");
        $("#id_talent_id").val(id)
    });

    $('#sendOffer').on("click", (e)=>{
        var id = $("#id_talent_id").val()
        console.log(id);
        $.ajax({
            type: "POST",
            url: $("#offerForm").attr('action'),
            data: $("#offerForm").serialize(),
            success: (data) => {
                $("#offerModal").modal('hide');
                $(`[data-talent='${id}']`).remove();
            },
            error: (data) => {
                alert(data.error);
            }
        })
    });

});