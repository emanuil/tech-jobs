"use strict";

$(function () {

    // Company Dashboard
    $('.boostForm').submit(function(e){
        e.preventDefault();
        var url = $(this).attr("action");
        var job_id = $(this).attr("data-job-id");
        $.ajax({
            type: "POST",
            url: url,
            data: $(this).serialize(),
            success: function(data){
                if (data.error) {
                    alert(data.error);
                }
                else {
                alert(data.message);
                $("#boostJob"+job_id).remove();
                var credits = $("#dashboardCredits").text();
                $("#dashboardCredits").text(credits-0.5);
                }
            }
        });
    });

});
