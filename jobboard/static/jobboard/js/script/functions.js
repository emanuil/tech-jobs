"use strict";

$(function () {

    $('.keep-open').on({
        "shown.bs.dropdown": function() { $(this).attr('closable', false);},
        "click": function() {},
        "hide.bs.dropdown": function() { return $(this).attr('closable') == 'true';}
    });

    $('.keep-open .btn-filter').on({
        "click": function () {
            $(this).parent().attr('closable', true);
        }
    })

    var $rolesSelector = $('#rolesInput').select2({
        placeholder: "Roles",
        containerCssClass: 'filters-container',
    });

    var $skillsSelector = $('#skillInput').select2({
        placeholder: "Skills",
        containerCssClass: 'filters-container',
        ajax: {
            url: '/api/skills/',
            dataType: 'json',
        }
    });
    // Add Filters
    $('.filters').click(function (e) {
        var key = $(this).attr("data-key");
        var val = $(this).attr("data-value");
        var value = val.replace()
        if (key === 'published'){
            if ($('#' + key).length != 0){
                $('#' + key).remove();
                $('#input' + key).remove();
            }
            $("#active-filters").append(`<div class='active-filter' id="${key}" data-value=${value}>${value} <i class='fa fa-times fa-sm f-cancel' data-key='' aria-hidden='true'></i></div>`);
            $(".search-query").append(`<input type='hidden' name=${key} value='${value}' id=input${key}>`);
            return getJobs();
        }

        if ($("#" + key + value).length == 0) {
            $("#active-filters").append(`<div class='active-filter' id="${key}-${value}" data-key=${key}>${value} <i class='fa fa-times fa-sm f-cancel' data-key='' aria-hidden='true'></i></div>`);
            if ($('#input'+key).length){
                var new_value = $('#input'+key).val() + ',' + value;
                $('#input'+key).val(new_value);
            }
            else {
                $(".search-query").append(`<input type='hidden' name=${key} value='${value}' id=input${key}>`);
            }
            getJobs();
        }
    });

    // Remove Filters
    $("#active-filters").on("click", ".f-cancel", function () {
        var filter = $(this).parent().attr("id");
        $(this).parent().remove();
        if (filter === 'filterSalary'){
            $("#input_min_salary").remove();
            $("#input_max_salary").remove();
        }
        else if (filter.includes('roles')) {
            var role = filter.split(' ');
            var values = $('#inputRoles').val();
            var length = values.split(',').length;
            var value = role[1].replace(/_/g, " ");
            console.log(value);
            if (length != 1){
                $('#inputRoles').val(values.replace(value,'').replace(/(^[,\s]+)|([,\s]+$)/g, ''));
            }
            else {
                $('#inputRoles').remove();
            }
        }
        else if (filter.includes('skills')) {
            var skill = filter.split('_');
            var values = $('#inputSkills').val();
            var length = values.split(',').length;
            if (length != 1){
                $('#inputSkills').val(values.replace(skill[1],'').replace(/(^[,\s]+)|([,\s]+$)/g, ''));
            }
            else {
                $('#inputSkills').remove();
            }
        }
        else {
            var key = filter.split('-')[0];
            var filter = filter.split('-')[1];
            var values = $('#input'+key).val();
            var length = values.split(',').length;
            if (length != 1){
                $('#input'+key).val(values.replace(filter,'').replace(/(^[,\s]+)|([,\s]+$)/g, ''))
            }else{
                $("#input"+key).remove();
            }
        }
        getJobs();
    });

    // Salary Slider
    var slider = document.getElementById('salary_slider');

    noUiSlider.create(slider, {
        start:[0, 8000],
        connect: true,
        tooltips: [wNumb({decimals:0}),wNumb({decimals:0})],
        step: 100,
        range: {
            'min': 0,
            'max': 8000
        },
        format: wNumb({
            decimals: 0
        })
    });

    slider.noUiSlider.on('change', (e)=>{
        var salary_range = slider.noUiSlider.get();
        var min_salary = salary_range[0];
        var max_salary = salary_range[1];
        if (max_salary === '8000'){
            max_salary = '8000+'
        }
        if ($("#filterSalary").length != 0) {
            $("#filterSalary").remove();
            $("#input_min_salary").remove();
            $("#input_max_salary").remove();
        }
        $("#active-filters").append(`<div class='active-filter' id=filterSalary>${min_salary} - ${max_salary} BGN <i class='fa fa-times fa-sm f-cancel' data-key='' aria-hidden='true'></i></div>`);
        $(".search-query").append(`<input type='hidden' name=min_salary value=${min_salary} id=input_min_salary>`);
        $(".search-query").append(`<input type='hidden' name=max_salary value=${max_salary} id=input_max_salary>`);
        getJobs();
    });

    // Skills Filter
    $('#rolesInput').on('select2:select', (e)=>{
        var value = $('#rolesInput').select2('data')[0].text;
        var id = value.replace(/ /g, "_");
        if ($('#roles '+id).length === 0){
            $('#active-filters').append(`<div class='active-filter' id="roles ${id}">${value} <i class='fa fa-times fa-sm f-cancel' data-key='' aria-hidden='true'></i></div>`);
            if ($('#inputRoles').length){
                var new_value = $('#inputRoles').val() + ',' + value;
                $('#inputRoles').val(new_value);
            }
            else {
                $('.search-query').append(`<input type='hidden' name=roles value='${value}' id=inputRoles>`);
            }
            $rolesSelector.val(null).trigger('change');
            getJobs();
        }
        else {
            $rolesSelector.val(null).trigger('change');
        }
    });


    $('#skillInput').on('change', function(event) {
        var value = $(this).text();

        if ($("#skills_" + value).length == 0) {
            $("#active-filters").append(`<div class='active-filter' id='skills_${value}'>${value} <i class='fa fa-times fa-sm f-cancel' data-key='' aria-hidden='true'></i></div>`);
            if ($('#inputSkills').length){
                var new_value =  $("#inputSkills").val() +','+ value;
                $('#inputSkills').val(new_value);
            }
            else{
                $(".search-query").append(`<input type='hidden' name=skills value='${value}' id=inputSkills>`);
            }
            $(this).val("");
            $(this).text("");
            getJobs();
        }
        else{
            $(this).val("");
            $(this).text("");

        }
    });

    $('#mobileFilterBtn').on('click', (e)=> {
        $('#filter-panel').toggleClass('d-none');
    });

    $("#jobSearch").submit((e) =>{e.preventDefault(); getJobs();});

    function getJobs() {
        var query = $('#jobSearch').serialize();
        var url = '/api/jobs';
        $.ajax({
            type: "GET",
            url: url,
            data: query,
            dataType: 'json',
            success: function success(data) {
                $('#jobsList').empty();
                $.each(data, function (key, value) {
                    var title = `${value.title[0].toUpperCase()}${value.title.slice(1)}`;
                    var job_id = value.id;
                    var url = `/job/${job_id}/${value.slug}`;
                    var company = value.company.name;
                    var company_url = `/${value.company.slug}`;
                    var job_type = value.job_type;
                    var location = value.location;
                    var experience = value.experience;
                    var description = value.description.replace(/<[^>]+>/g, '').substring(0,200);
                    var skills = value.skills;
                    var min_salary = value.min_salary;
                    var max_salary = value.max_salary;
                    var currency = value.currency;
                    var role = value.role;
                    var featured = value.featured;
                    var publish_date = new Date(value.publish_date).toLocaleString('en-US',{month: 'long', day:'2-digit',  year:'numeric'});
                    var logo = value.company.logo;
                    var mob_salary = '';
                    var desktop_salary = '';
                    var logo_div='';
                    if (featured){
                        var heading = `<div class="card"><div class="card-header featured-job">Featured</div>`;
                    }else {
                        var heading = '<div class="card">';
                    }
                    if (min_salary !== null || max_salary !== null){
                        if (min_salary == null){
                            min_salary = '';
                        }
                        if (max_salary == null){
                            max_salary = '';
                        }
                        var mob_salary = `<div class="listing-salary">
          <span class="">${min_salary} - ${max_salary} ${currency}</span></div>`
                        var desktop_salary = `<div class="noMP col-sm-3"><a href="${ url }">
                        <div class="listing-type">
                        <div class="listing-salary">
                            <span class="">${min_salary} - ${max_salary} ${currency}</span></div>
                        </div></a></div>`;
                    }
                    if (logo !== null){
                        var logo_div = `<a href="${company_url}" target="_blank">
                        <div class="listing-logo">
                        <img src="${logo}"/>
                        </div></a>`
                    }
                    $('#jobsList').append(heading + `
    <div class="job-card">
    <div class="d-block d-sm-none job-mobile">
      <a href="${url}">
      <div class="recommendation-title">${title}</div>

        <div class="job-detail">
        <i class="fa fa-building list-fa fa-lg" aria-hidden="true"></i><span class="">${company}</span></div>
        <div class="job-detail"><i class="fa fa-briefcase list-fa fa-lg" aria-hidden="true"></i><span>${job_type}</span>
        </div>
        <div class="job-detail">
          <i class="fa fa-map-marker list-fa fa-lg" aria-hidden="true"></i><span class="">${location}</span></div>
         <div class="job-detail">
          <i class="fa fa-calendar list-fa fa-lg" aria-hidden="true"></i><span class="">${publish_date}</span>
         </div>`+mob_salary+`</a>
    </div>
    <div class="d-none d-sm-block">
        <div class="row">
        <div class="noMP col-sm-9">`+logo_div+`
            <a href="${url}">
            <div class="listing-title">${title}</div>
            <div class="listing-info">
                <i class="fa fa-building list-fa fa-lg" aria-hidden="true"></i><span class="">${company}</span>
                <i class="fa fa-briefcase list-fa fa-lg" aria-hidden="true"></i><span>${job_type}</span>
                <i class="fa fa-map-marker list-fa fa-lg" aria-hidden="true"></i><span class="">${location}</span>
                <i class="fa fa-calendar list-fa fa-lg" aria-hidden="true"></i><span class="">${publish_date}</span>
            </div>
            </a>
        </div>`+desktop_salary+`</a>
    </div>
        <div class="noMP col-sm-12">
            <a href="${ url }">
            <div class="listing-desc">
                ${description}...
            </div>
            </a>
        </div>
    </div>
</div>
</div>
`);
                });
            }
        });
    }

});












