"use strict";

$(function () {

    $('#id_roles').select2();

    $('#id_role').select2({
        placeholder: '',
        containerCssClass: 'form-control'
    });

    $('#id_skills').select2({
        tags: true,
        ajax: {
            url: '/api/skills/',
            dataType: 'json',
            processResults: (data)=>{
            var text = $.map(data.results, (obj)=>{
                obj.id = obj.text;
                return obj;
            });
            return {
                results: text
            }
        }
        },

    });
});