"use strict";

$(function () {

    // Payments Page
    $("#CreditsForm").submit(function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: 'pricing/',
            data: $(this).serialize(),
            success: (data) =>{
                if (data.currency === 'BGN'){
                    $('.price-box').text(data.price + ' lv.')
                }
                $('#pricingSegment').removeClass('hidden');
            }
        });
    });

    $('#PaymentButton').on("click", function(e){
        e.preventDefault();
        var credits = $('#id_credits').val()
        var csrftoken = Cookies.get('csrftoken')
        $.ajax({
            type: "POST",
            url: "purchase/",
            data: {'credits': credits, 'csrfmiddlewaretoken': csrftoken},
            success: (data) => {
                console.log(data)
                $('#encoded').val(data.encoded);
                $('#checksum').val(data.checksum);
                $('#PaymentForm').submit();
            }
        });
    });

});
