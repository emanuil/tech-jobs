import datetime
import decimal
import hmac
import base64
import json
from hashlib import sha1

from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import View,TemplateView
from django.views.generic.edit import CreateView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.db import transaction
from django.db.models import Q
from django.core.mail import send_mail, send_mass_mail, EmailMessage, EmailMultiAlternatives
from django.template.loader import render_to_string
from django.views.decorators.http import require_POST
from django.http import JsonResponse, HttpResponseForbidden, HttpResponse, HttpResponseBadRequest
from django.core import serializers
from django.core.exceptions import ImproperlyConfigured
from django.utils import six
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.db.models import Avg
from django.utils.timezone import now
from django.http import HttpResponseRedirect

from allauth.account.views import SignupView
from rest_framework import generics
from dal import autocomplete
from taggit.models import Tag
#from pinax.blog.models import Post
from .models import Applicant, Company, JobPost, Application, Subscriber, Purchase, Salary, Offer
from content.models import BlogPost
from .forms import CompanySignupForm, CompanyUpdateForm, ApplicantSignupForm, ApplicantUpdateForm, NewJobPostForm, JobPostUpdateForm, UserUpdateForm, CompanySubscribeForm, JobPostFilterForm, PricingForm, EditApplicationForm, DirectApplicantForm, LeadForm, UserSignupForm, ApplicantSearchForm, OfferForm, SalarySearchForm
from jobboard.tasks import save_query_task
from .filters import CompanyFilter, JobPostFilter
from .serializers import CompanySerializer, JobPostSerializer, ApplicationSerializer
from .utils import calculate_price

###########################
# Main Views
###########################
def home(request):
    if request.GET.get('q') or request.GET.get('q') == '':
        search = request.GET.get('q')
        return redirect('/jobs/?{}={}'.format('q', search))

    try:
        recent_blogs = None #BlogPost.objects.order_by("-timestamp")[:3]
    except IndexError:
        recent_blogs = None

    context = {'page_title': 'Tech Hire | The Best Technical Jobs and Candidates in One Place', 'description': 'Search the top technical jobs in Bulgaria and discover the next great opportunity. Apply to roles at the best technology companies and startups with one click.', 'keywords': 'Tech, Jobs, Developer, Designer, Technology, Recruitment, Job',
        'recent_blogs': recent_blogs}

    if hasattr(request.user, 'applicant'):
        applicant = request.user.applicant
        if applicant.last_query:
            recommended_jobs = JobPost.objects.search(q=applicant.last_query).select_related()
            context['jobs'] = [job for job in recommended_jobs if job.min_salary and job.max_salary and job.currency][:4]
            return render(request, 'index.html', context)

    elif hasattr(request.user, 'company'):
        return render(request, 'index.html', context)

    recent_jobs = JobPost.objects.search().select_related()
    context['jobs'] = [job for job in recent_jobs if job.min_salary and job.max_salary and job.currency][:4]
    return render(request, 'index.html', context)


def jobs_search(request):
    jobs = JobPost.objects.search(**request.GET).select_related('company')

    search = request.GET.get('q')

    if hasattr(request.user, 'applicant') and search:
        applicant = request.user.applicant
        save_query_task.delay(applicant.id, search)

    experience = JobPost.EXPERIENCE
    job_type = JobPost.JOB_TYPE
    roles = JobPost.ROLES
    location = JobPost.LOCATION
    published = JobPost.PUBLISHED
    currency = JobPost.CURRENCIES
    form = JobPostFilterForm()
    description = 'Find software developer, designer, tech support and many other technical jobs at top startups and tech companies.'
    keywords = ' Jobs,'.join(role[0] for role in roles)
    context = {'jobs': jobs, 'search': search, 'exp': experience, 'job_type': job_type, 'roles': roles, 'location': location, 'published': published, 'currency':currency, 'slider': True, 'multiple': True, 'js': 'jobboard/js/functions.min.js', 'page_title': 'Jobs | Find your role at the best tech companies | Tech Hire', 'description': description, 'keywords': keywords}
    return render(request, 'jobs.html', context)


def jobpost(request, id, slug=None):
    jobpost = get_object_or_404(JobPost, pk=id)

    if jobpost.slug != slug:
        return redirect('job', id=id, slug=jobpost.slug)

    if jobpost.expired:
        return render(request, 'jobpost_expired.html', {'expired_job':jobpost.title})

    skills = jobpost.skills.names()
    keywords = ','.join(skills)
    title = '{} | Tech Hire'.format(jobpost)
    description = 'View job details and apply for {0} at {1} on Tech Hire https://techhire.bg{2}. Skills: {3} '.format(jobpost.title, jobpost.company,request.path, keywords)
    context = {'jobpost': jobpost, 'page_title': title, 'description': description, 'keywords': keywords}

    if request.method == 'POST':
        if request.user.is_authenticated:
            user = request.user
            if hasattr(user, 'applicant'):
                applicant = user.applicant
                if not applicant.resume:
                    context['error'] = 'Please add your CV to your account to be able to apply.'
                    context['js'] = 'jobboard/js/jobpage.min.js'
                    return render(request, 'jobpost.html', context)
                application, created = Application.objects.get_or_create(
                    applicant=applicant, job_post=jobpost, defaults={'resume':applicant.resume})
                context['application'] = application
                context['js'] = 'jobboard/js/jobpage.min.js'
                return render(request, 'jobpost.html', context)
            else:
                context['error'] = 'Please log in or register as an applicant to apply.'
                context['js'] = 'jobboard/js/jobpage.min.js'
                return render(request, 'jobpost.html', context)
        else:
            return redirect('apply-direct', id=id , slug=slug)

    return render(request, 'jobpost.html', context)


def apply_direct(request, id, slug=None):
    jobpost = get_object_or_404(JobPost, pk=id)
    form = DirectApplicantForm()
    context = {'form': form, 'js':'jobboard/js/profile1.min.js','multiple':True}
    if request.method == 'POST':
        form = DirectApplicantForm(request.POST, request.FILES)
        user = authenticate(email=form.data['email'], password=form.data['password1'], backend='django.contrib.auth.backends.ModelBackend')

        if user is not None:
            if hasattr(user, 'applicant'):
                application, created = Application.objects.get_or_create(applicant=user.applicant, job_post=jobpost, defaults={'resume':request.FILES.get('resume', user.applicant.resume)})
                login(request, user, backend='django.contrib.auth.backends.ModelBackend')
                context.update({'applied': True, 'form': form, 'application': application, 'jobpost': jobpost})
                return render(request, 'apply_direct.html', context)
            else:
                context['errors'] = 'You cannot apply from a company account.'
                return render(request,'apply_direct.html', context)

        elif user is None and form.is_valid():
            form.cleaned_data['location'] = jobpost.location
            applicant = form.save(request)
            application, created = Application.objects.get_or_create(applicant=applicant, job_post=jobpost, defaults={'resume':applicant.resume})
            login(request, applicant.user, backend='django.contrib.auth.backends.ModelBackend')
            context.update({'applied': True, 'form': form, 'application': application, 'jobpost': jobpost})
            return render(request, 'apply_direct.html', context)

        else:
            if 'resume' in form.errors:
                context['errors'] = "CV must be a PDF or Word Document!"
            context['form'] = form
            return render(request, 'apply_direct.html', context)
    return render(request, 'apply_direct.html', context)

def earning(request):
    context = {'form': LeadForm, 'js':'jobboard/js/profile1.min.js', 'multiple':True}
    return render(request, 'landing_earning.html', context)


def landing_talent(request):
    context = {'form': UserSignupForm, 'js':'jobboard/js/profile1.min.js', 'multiple':True}
    return render(request, 'landing_signup.html', context)


@require_POST
def earning_success(request):
    form = LeadForm(request.POST)
    if form.is_valid():
        salaries = Salary.objects.filter(role=form.cleaned_data['role'], experience=form.cleaned_data['experience'], location='Sofia')
        context =  {'salaries': salaries, 'role': form.cleaned_data['role']}
        context['form'] = UserSignupForm()
        if not salaries:
            return render(request, 'landed.html', context)
        length = len(salaries)/2
        if length % 2 != 0:
            upper = sorted([s.salary for s in salaries])[-int(length+0.5):]
        else:
            upper = sorted([s.salary for s in salaries])[-int(length):]
        context['average'] = int(sum(upper)/len(upper))
        return render(request, 'landed.html', context)
    else:
        return redirect('landing-earning')


def company_list(request):
    f = CompanyFilter(request.GET, queryset=Company.objects.all().order_by('-last_job_post_date'))
    context = {'filter': f, 'page_title': 'Companies | Tech Hire', 'description': 'Browse top tech startups and companies and find your dream job.'}
    return render(request, 'company_list.html', context)


@login_required
def offer(request, pk):
    offer = get_object_or_404(Offer.objects.select_related(), pk=pk)
    if request.user == offer.applicant.user or request.user == offer.company.user:
        if request.method == 'POST' and request.user == offer.applicant.user:
            offer.interested = True
            offer.save()
            subject = 'A Candidate is interested in your job offer for {}'.format(offer.title)
            context = {'applicant':offer.applicant, 'company': offer.company, 'offer': offer}
            text_content = render_to_string('email/offer_accepted.txt', context)
            html_content = render_to_string('email/offer_accepted.html', context)
            mail = offer.applicant.user.email
            msg = EmailMultiAlternatives(subject, text_content, 'emanuil@techhire.bg', [mail])
            msg.attach_alternative(html_content, 'text/html')
            msg.send()
            return render(request, 'offer_page.html', {'offer': offer,'applicant': offer.applicant})
        return render(request, 'offer_page.html', {'offer': offer, 'applicant': offer.applicant})
    else:
        return redirect('home')

@login_required
@require_POST
def remove_offer(request, pk):
    offer = get_object_or_404(Offer.objects.select_related(), pk=pk)
    if request.user == offer.applicant.user:
        offer.active = False
        offer.save()
        return redirect('applicant-offers', id=offer.applicant.pk)
    elif request.user == offer.company.user:
        offer.active = False
        offer.save()
        return redirect('company-offers', slug=offer.company.slug)

def help(request):
    return render(request, 'help.html')

def prices(request):
    return render(request, 'prices.html')

def terms(request):
    return render(request, 'terms.html')


###########################
# Applicant Views
###########################
@login_required
def applicant_home(request, id):
    user = request.user
    applicant = get_object_or_404(Applicant, pk=id)
    companies = {offer.company for offer in applicant.offers.all()}
    if user == applicant.user:
        return render(request, 'applicant_home.html', {'applicant': applicant, 'owner': True})
    elif hasattr(user, 'company'):
        if user.company in companies:
            return render(request, 'applicant_home.html', {'applicant': applicant})
        else:
            return redirect('company-dashboard', slug=user.company.slug)
    else:
        return redirect('applicant-home', id=user.applicant.id)


@login_required
def applicant_update(request, id):
    user = request.user
    applicant = get_object_or_404(Applicant, pk=id)
    form = ApplicantUpdateForm(instance=applicant)

    if user == applicant.user:
        if request.method == 'POST':
            form = ApplicantUpdateForm(request.POST, request.FILES, instance=applicant)
            if form.is_valid():
                applicant = form.save(request)
                return redirect('applicant-home', id=id)
        context = {'form': form, 'js': 'jobboard/js/profile1.min.js', 'multiple': True}
        return render(request, 'applicant_update.html', context)
    else:
        return redirect('applicant-home', id=user.applicant.id)

class UserSignupView(SignupView):
    template_name = 'user_signup.html'
    form_class = UserSignupForm

    def get_context_data(self, **kwargs):
        context = super(UserSignupView, self).get_context_data(**kwargs)
        context.update({'linked_event': True})
        return context

    def get_success_url(self):
        return '/signup/applicant/'


class ApplicantSignupView(CreateView):
    template_name = 'applicant_signup.html'
    form_class = ApplicantSignupForm

    def get_context_data(self, **kwargs):
        context = super(ApplicantSignupView, self).get_context_data(**kwargs)
        context.update({'linked_event': True, 'js':'jobboard/js/profile1.min.js','multiple':True})
        return context

    def form_valid(self, form):
        self.object = form.save(self.request.user)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return '/welcome/'


@login_required
def applicant_applications(request, id):
    user = request.user
    applicant = get_object_or_404(Applicant, pk=id)
    if user == applicant.user:
        apps = Application.objects.select_related().filter(applicant=applicant)
        return render(request, 'applicant_applications.html', {'applications': apps})
    else:
        return redirect('applicant-home', id=user.applicant.id)


@login_required
def salary_check(request, id):
    user = request.user
    applicant = get_object_or_404(Applicant, pk=id)
    if user == applicant.user:
        if applicant.current_salary:
            form = SalarySearchForm()
            role = request.GET.get('role') or applicant.role
            experience = request.GET.get('experience') or applicant.experience
            location = request.GET.get('location') or applicant.location
            entries = Salary.objects.filter(role=role, experience=experience, location=location)
            salaries = sorted([int(entry.salary) for entry in entries])
            if len(salaries):
                average_salary = int(sum(salaries)/len(salaries))
                max_salary = max(salaries)
                min_salary = min(salaries)
            else:
                average_salary = 0
                max_salary = 0
                min_salary = 0
            graph = [0,0,0,0,0,0]
            for salary in salaries:
                if salary < 1000:
                    graph[0] += 1
                elif salary < 2000:
                    graph[1] += 1
                elif salary < 3000:
                    graph[2] += 1
                elif salary < 4000:
                    graph[3] += 1
                elif salary < 5000:
                    graph[4] += 1
                else:
                    graph[5] += 1

            skills = applicant.skills.names()
            jobs = JobPost.objects.filter(role=applicant.role, location=applicant.location).select_related()[:3]
            skill_salaries = Applicant.objects.filter(skills__name__in=skills, location=applicant.location).values('skills__name').annotate(Avg('current_salary'))
            context = {'min': min_salary, 'max': max_salary, 'avg': average_salary, 'skill_salaries': skill_salaries, 'salaries': json.dumps(salaries), 'chart': True,'js': 'jobboard/js/chart.min.js', 'graph': json.dumps(graph), 'jobs': jobs, 'form': form, 'multiple': True, 'role': role, 'experience': experience, 'location': location}
            return render(request, 'salary_check.html', context)
        else:
            return render(request, 'salary_check.html')
    else:
        return redirect('applicant-home', id=user.applicant.id)

@login_required
def applicant_offers(request, id):
    applicant = get_object_or_404(Applicant, pk=id)
    if request.user == applicant.user:
        queryset = Offer.objects.filter(applicant=applicant, active=True)
        return render(request, 'offers_list.html', {'offers': queryset})
    else:
        return redirect('welcome')

###########################
# Company Views
###########################
def company_home(request, slug):
    user = request.user
    company = get_object_or_404(Company, slug=slug)

    if request.method == 'POST':
        form = CompanySubscribeForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data.get('email')
            subscribe, created = Subscriber.objects.get_or_create(company=company, email=email)
            message = 'You have subscribed.'
            return render(request, 'company_page.html',
                         {'company': company, 'message': message, 'form': form})
    jobs = JobPost.objects.filter(company=company, expired=False).order_by('-featured','-publish_date').select_related('company')
    form = CompanySubscribeForm()
    return render(request, 'company_page.html', {'company': company, 'form': form, 'jobs': jobs})


@login_required
def company_update(request, slug):
    user = request.user
    company = get_object_or_404(Company, slug=slug)
    if user == company.user:
        if request.method == 'POST':
            created = False if company.description else True
            form = CompanyUpdateForm(request.POST, request.FILES, instance=company)
            if form.is_valid():
                form.save()
                if created:
                    return redirect('welcome')
                return redirect('company-home', slug=slug)

        form = CompanyUpdateForm(instance=company)
        return render(request, 'company_update.html', {'form': form, 'multiple': True, 'js': 'jobboard/js/profile1.min.js'})
    else:
        return redirect('company-home', slug=slug)


class CompanySignupView(SignupView):
    template_name = 'company_signup.html'
    form_class = CompanySignupForm

    def get_context_data(self, **kwargs):
        context = super(CompanySignupView, self).get_context_data(**kwargs)
        context.update({'linked_event': True})
        return context

    def get_success_url(self):
        return '/{}/update/'.format(self.user.company.slug)


@login_required
def talent_search(request):
    company = get_object_or_404(Company, user=request.user)

    if not company.recruiter:
        return render(request, 'purchase_subscription.html')

    get_form = ApplicantSearchForm()
    post_form = OfferForm()
    post_form.helper.form_action = '/{}/make-offer/'.format(company.slug)
    params = {}
    if request.GET.get('role'):
        role = request.GET.get('role')
        get_form.fields['role'].initial = role
        params['role'] = role
        #params['roles__contains'] = [role]

    if request.GET.get('experience'):
        experience = request.GET.get('experience')
        get_form.fields['experience'].initial=experience
        params['experience'] = experience

    if request.GET.get('location'):
        location = request.GET.get('location')
        get_form.fields['location'].initial = location
        params['location'] = location

    if request.GET.get('skills'):
        skills = request.GET.getlist('skills')
        get_form.fields['skills'].initial= skills
        params['skills__name__in'] = skills

    context = {'get_form': get_form, 'multiple': True, 'js': 'jobboard/js/talent.min.js', 'form': post_form}
    if request.GET:
        applicants = Applicant.objects.filter(**params, looking=True).exclude(offers__company=company).distinct().order_by('-user__date_joined').prefetch_related('tagged_items__tag')
        context['talents']= applicants[:50]
    return render(request, 'talent_search.html', context)


@login_required
def jobpost_new(request, slug):
    user = request.user
    company = get_object_or_404(Company, slug=slug)

    if user == company.user:
        if company.credits < 1:
            return redirect('buy-credits', slug=slug)

        form = NewJobPostForm()
        context = {'form': form, 'company': company, 'multiple': True, 'js': 'jobboard/js/profile.min.js'}
        if request.method == 'POST':
            form = NewJobPostForm(request.POST)
            if form.is_valid():
                featured = form.cleaned_data.get('featured')
                if featured and company.credits >= 2:
                    with transaction.atomic():
                        jobpost = form.save(request)
                        company.credits -= 2
                        company.save()
                        return redirect('job', id=jobpost.id, slug=jobpost.slug)

                elif not featured and company.credits >= 1:
                    with transaction.atomic():
                        jobpost = form.save(request)
                        company.credits -= 1
                        company.save()
                        return redirect('job', id=jobpost.id, slug=jobpost.slug)

                else:
                    context['error'] = 'There was a problem posting your job.'
                    return render(request, 'jobpost_create.html', context)

        return render(request, 'jobpost_create.html', context)
    else:
        return redirect('company-home', slug=slug)


@login_required
def jobpost_update(request, slug, id):
    user = request.user
    company = get_object_or_404(Company, slug=slug)

    if user == company.user:
        jobpost = JobPost.objects.get(id=id)
        if request.method == 'POST':
            form = JobPostUpdateForm(request.POST, instance=jobpost)
            if form.is_valid():
                form.save()
                return redirect('job', id=id, slug=jobpost.slug)
        form = JobPostUpdateForm(instance=jobpost)
        context = {'form': form, 'js': 'jobboard/js/profile.min.js', 'multiple': True}
        return render(request, 'jobpost_update.html', context)
    else:
        return redirect('job', id=id, slug=jobpost.slug)


@login_required
@require_POST
def jobpost_repost(request, slug, id):
    user = request.user
    company = get_object_or_404(Company, slug=slug)

    if user == company.user and company.credits >= 1:
        jobpost = JobPost.objects.get(id=id)

        if jobpost.featured and company.credits >= 2:
            with transaction.atomic():
                jobpost.pk = None
                jobpost.expired = False
                company.credits -= 2
                jobpost.save()
                company.save()
                return redirect('job', id=jobpost.id, slug=jobpost.slug)
        elif not jobpost.featured and company.credits >= 1:
            with transaction.atomic():
                jobpost.pk = None
                jobpost.expired = False
                company.credits -= 1
                jobpost.save()
                company.save()
                return redirect('job', id=jobpost.id, slug=jobpost.slug)
    else:
        return redirect('buy-credits', slug=slug)


@login_required
def company_dashboard(request, slug):
    user = request.user
    company = get_object_or_404(Company, slug=slug)

    if user == company.user:
        if request.GET.get('dash_filter'):
            f = request.GET.get('dash_filter')
            jobposts = JobPost.objects.filter(company=company, title__icontains=f).prefetch_related('applications').order_by('-publish_date')
        else:
            jobposts = JobPost.objects.filter(company=company).prefetch_related('applications').order_by('-publish_date')
            f = None
        active = {jobpost for jobpost in jobposts if jobpost.expired is False}
        context = {'company': company, 'jobposts': jobposts, 'user': user,
                      'active': active, 'js': 'jobboard/js/dashboard.min.js',
                      'filter':f}

        return render(request, 'company_dashboard.html', context)
    else:
        return redirect('company-home', slug=slug)


@login_required
def company_jobpost(request, slug, id):
    user = request.user
    company = get_object_or_404(Company, slug=slug)

    if user == company.user:
        jobpost = get_object_or_404(JobPost, pk=id)
        applications = Application.objects.search(job_post=jobpost, **request.GET).exclude(status='Rejected').select_related()

        if request.GET.get('rejected') == 'true':
            applications = Application.objects.search(job_post=jobpost, **request.GET).select_related()

        interviews = applications.filter(status='Interview').exclude(interview_date__isnull=True)
        offers = applications.filter(status='Offer')
        hired = applications.filter(status='Hired')
        form = EditApplicationForm()
        context = {'applications': applications, 'jobpost': jobpost, 'interviews': interviews,'offers': offers, 'slug': slug, 'form': form, 'js': 'jobboard/js/applications.min.js', 'listjs': True, 'datepicker': True}
        return render(request, 'company_jobpost.html', context)
    else:
        return redirect('company-home', slug=slug)


@login_required
@require_POST
def make_offer(request, slug):
    company = get_object_or_404(Company, slug=slug)

    if request.user != company.user and not request.is_ajax():
        return HttpResponseForbidden()

    form = OfferForm(request.POST)
    applicant_id = request.POST.get('talent_id')
    applicant = get_object_or_404(Applicant, pk=applicant_id)
    if form.is_valid():
        offer = form.save(request, applicant)
        subject = 'New Job Offer from {}'.format(company.name)
        context = {'name':applicant.full_name, 'company': company.name, 'offer': offer}
        text_content = render_to_string('email/offer.txt', context)
        html_content = render_to_string('email/offer.html', context)
        mail = applicant.user.email
        msg = EmailMultiAlternatives(subject, text_content, 'emanuil@techhire.bg', [mail])
        msg.attach_alternative(html_content, 'text/html')
        msg.send()
        data = {'success': True}
        return JsonResponse(data, status=200)
    else:
        data = {'error': form.errors}
        return JsonResponse(data, status=500)


@login_required
@require_POST
def application_update(request, slug, id, pk):
    user = request.user
    company = get_object_or_404(Company, slug=slug)

    if user == company.user and request.is_ajax():
        application = Application.objects.get(pk=pk)
        form = EditApplicationForm(request.POST, instance=application)
        if form.is_valid():
            app = form.save()
            data = {'id': pk, 'status': app.status, 'rating': app.get_applicant_rating_display(), 'comment': app.comment, 'interview': app.interview_date}
            return JsonResponse(data, status=200)
        else:
            data = {'error': form.errors}
            return JsonResponse(data, status=500)
    else:
        return HttpResponseForbidden()


@login_required
@require_POST
def jobpost_boost(request, slug, id):
    company = get_object_or_404(Company, slug=slug)
    user = get_object_or_404(User, pk=request.POST.get("user_id"))
    amount = decimal.Decimal('0.5')
    if user == company.user and request.is_ajax():
        jobpost = JobPost.objects.get(pk=id)
        if jobpost.boosted:
            data = {'error': 'Job has been boosted already.'}
            return JsonResponse(data)
        if company.credits >= amount:
            with transaction.atomic():
                jobpost.boosted = True
                jobpost.publish_date = now()
                jobpost.save()
                company.credits -= amount
                company.save()
                data = {'status': 200, 'message': 'Job has been boosted!'}
                return JsonResponse(data)
        else:
            data = {'error': 'Insufficient credits. Please purchase more credits.'}
            return JsonResponse(data)
    else:
        return HttpResponseForbidden()

@login_required
@require_POST
def pricing(request, slug):
    user = request.user
    company = get_object_or_404(Company, slug=slug)

    if user == company.user and request.is_ajax():
        credits = int(request.POST['credits'])
        new_form = request.POST.copy()
        new_form.update({'credits': credits})
        form = PricingForm(data=new_form)
        if form.is_valid():
            credits = form.cleaned_data.get('credits')
            price = calculate_price(credits)
            currency = form.cleaned_data.get('currency', 'BGN')
            data = {'status': 'success', 'price': price, 'currency': 'BGN'}
            return JsonResponse(data, status=200)
        else:
            data = {'status': 'error'}
            return JsonResponse(data)
    else:
        return HttpResponseForbidden()


@login_required
@require_POST
def new_purchase(request, slug):
    user = request.user
    company = get_object_or_404(Company, slug=slug)

    if user == company.user and request.is_ajax():
        credits = int(request.POST['credits'])
        price = calculate_price(credits)
        purchase = Purchase(
        company=company,
        credits=credits,
        amount=price,
        date=datetime.date.today(),
        )
        purchase.save()
        ENCODED = purchase.generate_order()
        CHECKSUM = hmac.new(settings.EPAY_SECRET.encode(), ENCODED, sha1).hexdigest()
        data = {'encoded': ENCODED.decode('utf-8'), 'checksum': CHECKSUM}
        return JsonResponse(data, status=200)
    else:
        return HttpResponseForbidden()


@login_required
def buy_credits(request, slug):
    user = request.user
    company = get_object_or_404(Company, slug=slug)

    if user == company.user:
        form = PricingForm()
        context = {'company': company, 'form':form, 'slug': slug, 'js': 'jobboard/js/payment.min.js'}
        return render(request, 'company_credits.html', context)
    else:
        return redirect('company-home', slug=slug)


@csrf_exempt
@require_POST
def payment_response(request):
    ENCODED = request.POST['encoded']
    checksum = request.POST['checksum']

    verify = hmac.new(settings.EPAY_SECRET.encode(), ENCODED.encode('utf-8'), sha1).hexdigest()

    if verify == checksum:
        payload = base64.b64decode(ENCODED)
        lines = str(payload,'utf-8').splitlines()
        print(payload)
        values = {line.split(':')[0].split('=')[1]:line.split(':')[1].split('=')[1] for line in lines}

        response = []
        print(values)
        for invoice, status in values.items():
            purchase = Purchase.objects.filter(invoice=invoice).first()
            if not purchase:
                response.append('INVOICE={}:STATUS=NO'.format(invoice))

            if status == 'PAID':
                if purchase.completed == False:
                    try:
                        purchase.payment_completed()
                        response.append('INVOICE={}:STATUS=OK'.format(invoice))
                    except Exception as e:
                        print(e)
                        response.append('INVOICE={}:STATUS=ERR'.format(invoice))

            elif status == 'DENIED' or status == 'EXPIRED':
                try:
                    purchase.delete()
                    response.append('INVOICE={}:STATUS=OK'.format(invoice))
                except Exception as e:
                    print(e)
                    response.append('INVOICE={}:STATUS=ERR'.format(invoice))

        print(':'.join(response))
        return HttpResponse(':'.join(response))

    else:
        response = 'ERR=CHECKSUM'
        return HttpResponse(response)


@login_required
def account_update(request, id):
    user = request.user
    account = get_object_or_404(User, pk=id)

    if user == account:
        if request.method == 'POST':
            form = UserUpdateForm(request.POST, instance=account)
            if form.is_valid():
                form.save(request.POST)
                message = 'Account updated!'
                return render(request, 'account_update.html',
                    {'form': form, 'message': message})
        form = UserUpdateForm(instance=account)
        return render(request, 'account_update.html', {'form': form})
    else:
        return redirect('account-update', id=user.id)


def registration(request):
    if request.user.is_authenticated:
        return redirect('account_logout')
    return render(request, 'registration.html')


def alerts_unsubscribe(request, slug, pk):
    sub = get_object_or_404(Subscriber, pk=pk)
    sub.delete()
    return HttpResponse('You have been unsubscribed.')


def company_offers(request, slug):
    company = get_object_or_404(Company, slug=slug)
    if request.user == company.user:
        queryset = Offer.objects.filter(company=company, active=True)
        return render(request, 'offers_list.html', {'offers': queryset})
    else:
        return redirect('company-home', slug=slug)

###########################
# API
###########################
class JobListAPI(generics.ListAPIView):
    serializer_class = JobPostSerializer

    def get_queryset(self):
        return JobPost.objects.search(**self.request.GET).select_related('company')


class ApplicationAPI(generics.ListAPIView):
    serializer_class = ApplicationSerializer

    def get_queryset(self):
        return Application.objects.search(**self.request.GET).select_related()


class SkillsAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs =Tag.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs
