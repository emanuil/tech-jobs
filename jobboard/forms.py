import datetime

from django.forms import Form, ModelForm, CharField, MultipleChoiceField, ChoiceField, HiddenInput, IntegerField, BooleanField
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from django.core.exceptions import ValidationError
from django.utils.timezone import now

from allauth.account.forms import SignupForm
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Field, Layout, ButtonHolder, Div, Button, HTML
from dal import autocomplete

from jobboard.models import Applicant, JobPost, Application, Company, Purchase, Subscriber, Salary, Lead, Offer
from taggit.models import Tag


class UserSignupForm(SignupForm):
    full_name = CharField(max_length=100)
    site_terms = BooleanField(required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_method = 'POST'
        self.helper.form_action = 'user-signup'
        self.fields['email'].label = 'Email'
        self.fields['full_name'].label = 'Full Name'
        self.fields['site_terms'].label = 'Terms of Services and Privacy Policy.'
        self.helper.layout = Layout(
            Div(
                Div('full_name', css_class='col-sm-12'),
                Div('email', css_class='col-sm-12'),
                Div('password1', css_class='col-sm-12'),
                css_class='row'),
            Div(
                Div('site_terms', css_class='col-sm-12'),
                css_class='row'),
            Div(
                Submit('submit', 'Sign Up', css_class='btn-action'),
                css_class='save-button'))

    def save(self, request):
        user = super(UserSignupForm, self).save(request)
        user.first_name = self.cleaned_data.get('full_name')
        user.save()
        return user


class ApplicantSignupForm(ModelForm):

    class Meta:
        model = Applicant
        fields = ['resume', 'role', 'experience', 'current_salary', 'skills', 'location']
        widgets = {
            'skills': autocomplete.TaggitSelect2('autocomplete-skills')
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_method = 'POST'
        self.fields['resume'].label = 'CV'
        self.fields['resume'].help_text = 'You need to upload a CV to apply for jobs. CV must be a PDF or Word Document!'
        self.fields['resume'].required = False
        self.fields['role'].label = 'Current Role'
        self.fields['role'].help_text = 'Select your current role.'
        self.fields['skills'].help_text = 'Search and add Programming Languages, Technologies, Foreign Languages and other relevant skills you possess.'
        self.fields['skills'].name = 'skills[]'
        self.fields['experience'].help_text = 'Years of experience in your current role.'
        self.fields['current_salary'].label = 'Current Salary'
        self.fields['current_salary'].help_text = 'Add your current monthly salary in BGN to see market averages. Your current salary is always kept private and only visible to you.'

        self.helper.layout = Layout(
            Div(
                Div('role', css_class='col-sm-6'),
                Div('experience', css_class='col-sm-6'),
                css_class='row'),
            Div(
                Div(Field('skills',css_class='js-example-basic-multiple'), css_class='col-sm-12'),
                css_class='row'),
            Div(
                Div('resume', css_class='col-sm-4'),
                Div('location', css_class='col-sm-4'),
                Div('current_salary', css_class='col-sm-4'),
                css_class='row'),
            Div(
                Submit('register', 'Register'),
                css_class='save-button'))

    def save(self, user):
        applicant = Applicant(
            user=user,
            full_name=user.first_name,
            resume = self.cleaned_data.get('resume'),
            current_salary=self.cleaned_data.get('current_salary'),
            role=self.cleaned_data.get('role'),
            experience=self.cleaned_data.get('experience'),
            location=self.cleaned_data.get('location'),
            )
        applicant.save()
        if self.cleaned_data.get('resume'):
            applicant.resume_upload_date = datetime.date.today()

        applicant.skills.add(*self.cleaned_data.get('skills'))
        applicant.save()

        current_salary = self.cleaned_data.get('current_salary')
        if current_salary:
            s = Salary(role=self.cleaned_data['role'], experience=self.cleaned_data['experience'], salary=current_salary, location=self.cleaned_data['location'])
            s.save()

        return user


class CompanySignupForm(SignupForm, ModelForm):
    first_name = CharField(max_length=50)
    last_name = CharField(max_length=50)
    site_terms = BooleanField(required=True)

    class Meta:
        model = Company
        fields = ['name', 'phone', 'marketing_consent']

    def __init__(self, *args, **kwargs):
        super(CompanySignupForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_method = 'POST'
        self.fields['phone'].help = 'Phone number for internal contact, not displayed publicly.'
        self.fields['email'].label = 'Email'
        self.fields['email'].help_text= 'Email used for account login.'
        self.fields['first_name'].label = 'First Name'
        self.fields['last_name'].label = 'Last Name'
        self.helper.layout = Layout(
            Div(
                Div('name', css_class='col-sm-6'),
                Div('phone', css_class='col-sm-6'),
                css_class='row'),
            Div(
                Div('email', css_class='col-sm-6'),
                Div('password1', css_class='col-sm-6'),
                css_class='row'),
            Div(
                Div('first_name', css_class='col-sm-6'),
                Div('last_name', css_class='col-sm-6'),
                css_class='row'),
            Div(
                Div('site_terms', css_class='col-sm-6'),
                css_class='row'),
            Div(
                Submit('register', 'Register'),
                css_class='save-button')
            )

    def save(self, request):

        user = super(CompanySignupForm, self).save(request)

        company = Company(
            user=user,
            name=self.cleaned_data.get('name'),
            slug=slugify(self.cleaned_data.get('name')),
            phone=self.cleaned_data.get('phone'),
            )
        company.save()

        return user


class ApplicantUpdateForm(autocomplete.FutureModelForm):
    roles = MultipleChoiceField(choices=Applicant.ROLES)

    class Meta:
        model = Applicant
        exclude = ['user', 'last_application_date', 'resume_upload_date', 'last_query_date', 'full_name']
        widgets = {
            'skills': autocomplete.TaggitSelect2('autocomplete-skills')
        }

    def __init__(self, *args, **kwargs):
        super(ApplicantUpdateForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_method = 'POST'
        self.helper.form_show_errors = True
        self.fields['looking'].label = 'Open to Opportunities'
        self.fields['looking'].help_text = 'Keep selected if you would like to receive offers with salaries from companies!'
        self.fields['resume'].required = False
        self.fields['resume'].help_text = 'You need to upload a CV to apply for jobs. CV must be PDF or Word Document!'
        self.fields['role'].label = 'Current Role'
        self.fields['role'].help_text = 'Select your current role.'
        self.fields['roles'].required = False
        self.fields['roles'].label = 'Other Roles'
        self.fields['roles'].help_text = 'Select other roles you are interested in.'
        self.fields['skills'].help_text = 'Search and add Programming Languages, Technologies, Foreign Languages and other relevant skills you possess.'
        self.fields['skills'].name = 'skills[]'
        self.fields['experience'].help_text = 'Years of experience in your current role.'
        self.fields['job_type'].label = 'Job Type'
        self.fields['current_salary'].label = 'Current Salary'
        self.fields['current_salary'].help_text = 'Add your current monthly salary in BGN to see market averages. Your current salary is always kept private and only visible to you.'
        self.fields['desired_salary'].label = 'Desired Salary'
        self.fields['desired_salary'].help_text = 'Enter the salary you would like to receive in your next role.'
        self.helper.layout = Layout(

            Div(
                Div('looking', css_class='col-sm-12'),
                Div('role', css_class='col-sm-4'),
                Div('experience', css_class='col-sm-4'),
                Div('current_salary', css_class='col-sm-4'),
                css_class='row'),
            Div(
                Div(Field('skills',css_class='js-example-basic-multiple'), css_class='col-sm-12'),
                css_class='row'),
            Div(
                Div('location', css_class='col-sm-4'),
                Div(Field('roles', css_class='js-example-basic-multiple'), css_class='col-sm-8'),
                css_class='row'),
            Div(
                Div('resume', css_class='col-sm-4'),
                Div('job_type', css_class='col-sm-4'),
                Div('desired_salary', css_class='col-sm-4'),
                css_class='row'),
            Div(
                Submit('save', 'Save'),
                css_class='save-button'))

    def clean_current_salary(self):
        current_salary = self.cleaned_data.get('current_salary')
        if current_salary:
            if current_salary < 500 or current_salary > 25000:
                raise ValidationError('Enter a valid monthly salary in BGN!')
        return current_salary

    def save(self, request):
        applicant = super(ApplicantUpdateForm, self).save(request)
        resume = self.cleaned_data.get('resume')
        if 'current_salary' in self.changed_data:
            current_salary = self.cleaned_data.get('current_salary')
            s = Salary(role=self.cleaned_data['role'], experience=self.cleaned_data['experience'], salary=current_salary, location=self.cleaned_data['location'])
            s.save()

        if resume:
            applicant.resume_upload_date = datetime.date.today()
            applicant.save()

        return applicant


class CompanyUpdateForm(ModelForm):

    class Meta:
        model = Company
        exclude = ['user', 'credits', 'last_job_post_date',
        'recruiter', 'recruiter_expiry_date']
        widgets = {'skills': autocomplete.TaggitSelect2('autocomplete-skills')}

    def __init__(self, *args, **kwargs):
        super(CompanyUpdateForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_method = 'POST'
        self.fields['address'].label = 'Office address'
        self.fields['phone'].help_text = 'Phone number for internal contact, not displayed publicly.'
        self.fields['notification_email'].help_text = 'Applications & CVs would be sent to this address if specified.'
        self.fields['background'].help_text = 'Recommended size 1000x220 pixels.'
        self.fields['logo'].help_text = 'Recommended size 160x160 pixels.'
        self.fields['benefits'].help_text = 'Add benefits that you offer separated by commas. Will be dispalyed on company careers page.'
        self.fields['skills'].label = 'Technologies'
        self.fields['skills'].help_text= 'Search and add programming languages, frameworks, libraries and other technologies that are used in your company.'
        self.helper.layout = Layout(
            Div(
                Div('name', css_class='col-sm-6'),
                Div('website', css_class='col-sm-6'),
                css_class='row'),
            Div(
                Div('phone', css_class='col-sm-6'),
                Div('address', css_class='col-sm-6'),
                css_class='row'),
            Div(
                Div('notification_email', css_class='col-sm-4'),
                Div('city', css_class='col-sm-4'),
                Div('team_size', css_class='col-sm-4'),
                css_class='row'),
            Div(
                Div('logo', css_class='col-sm-6'),
                Div('background', css_class='col-sm-6'),
                css_class='row'),
            Div(Field('skills', css_class='js-example-basic-multiple')),
            Div('benefits'),
            Div('description'),
            Div(
                Submit('save', 'Save'),
                css_class='save-button')
            )


class NewJobPostForm(ModelForm):

    class Meta:
        model = JobPost
        exclude = ['company', 'slug', 'expired', 'boosted', 'expiry_date',
                   'publish_date']
        widgets = {'role': autocomplete.Select2(),
        'skills': autocomplete.TaggitSelect2('autocomplete-skills')}

    def __init__(self, *args, **kwargs):
        super(NewJobPostForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_method = 'POST'
        self.fields['featured'].help_text = 'Mark the job as featured to promote it ahead of other jobs. Featured jobs cost 2 credits.'
        self.fields['job_type'].label = 'Type of Job'
        self.fields['role'].help_text = 'Select the role you are recruiting for.'
        self.fields['job_type'].help_text = 'Select the type of employment.'
        self.fields['min_salary'].label = 'Minimum Monthly Salary'
        self.fields['max_salary'].label = 'Maximum Monthly Salary'
        self.fields['skills'].help_text = 'Add the skills you are looking for in a candidate.'
        self.helper.layout = Layout(
            Div(
                Div('title', css_class='col-sm-8'),
                Div('featured', css_class='col-sm-4'),
                css_class='row'),
            Div(
                Div(Field('role',css_class='js-example-basic-single'), css_class='col-sm-4'),
                Div('job_type', css_class='col-sm-4'),
                Div('experience', css_class='col-sm-4'),
                css_class='row'),
            Div(
                Div('min_salary', css_class='col-sm-5'),
                Div('max_salary', css_class='col-sm-5'),
                Div('currency', css_class='col-sm-2'),
                css_class='row'),
            Div(
                Div(Field('skills',css_class='js-example-basic-multiple'), css_class='col-sm-8'),
                Div('location', css_class='col-sm-4'),
                css_class='row'),
            Div('description'),
            Div(Submit('submit', 'Post Job'),
                 css_class='save-button')
            )

    def save(self, request):

        company = request.user.company

        today = now()

        jobpost = JobPost(
            title=self.cleaned_data.get('title'),
            slug=slugify(self.cleaned_data.get('title') + ' | ' + company.name),
            featured=self.cleaned_data.get('featured'),
            company=company,
            job_type=self.cleaned_data.get('job_type'),
            min_salary=self.cleaned_data.get('min_salary'),
            max_salary=self.cleaned_data.get('max_salary'),
            currency=self.cleaned_data.get('currency'),
            role=self.cleaned_data.get('role'),
            location=self.cleaned_data.get('location'),
            experience=self.cleaned_data.get('experience'),
            description=self.cleaned_data.get('description'),
            publish_date=today,
            expiry_date=today.date() + datetime.timedelta(days=30),
            boosted=False,
            expired=False,
            )
        jobpost.save()

        jobpost.skills.add(*self.cleaned_data.get('skills'))

        company.last_job_post_date = today
        company.save()
        return jobpost


class JobPostUpdateForm(ModelForm):

    class Meta:
        model = JobPost
        exclude = ['company', 'slug', 'expired', 'boosted', 'expiry_date',
                    'featured', 'publish_date']
        widgets = {'role': autocomplete.Select2(),
        'skills': autocomplete.TaggitSelect2('autocomplete-skills')}


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_method = 'POST'
        self.fields['job_type'].label = 'Type of Job'
        self.fields['role'].help_text = 'Select the role you are recruiting for.'
        self.fields['min_salary'].label = 'Minimum Monthly Salary'
        self.fields['max_salary'].label = 'Maximum Monthly Salary'
        self.fields['skills'].help_text = 'Search and add the skills you are looking for in a candidate.'
        self.helper.layout = Layout(
            Div('title'),
            Div(
                Div(Field('role',css_class='js-example-basic-single'), css_class='col-sm-4'),
                Div('job_type', css_class='col-sm-4'),
                Div('experience', css_class='col-sm-4'),
                css_class='row'),
            Div(
                Div('min_salary', css_class='col-sm-5'),
                Div('max_salary', css_class='col-sm-5'),
                Div('currency', css_class='col-sm-2'),
                css_class='row'),
            Div(
                Div(Field('skills',css_class='js-example-basic-multiple'), css_class='col-sm-8'),
                Div('location', css_class='col-sm-4'),
                css_class='row'),
            Div('description'),
            Div(Submit('submit', 'Save Job'),
                 css_class='save-button')
            )


class JobPostFilterForm(autocomplete.FutureModelForm):

    class Meta:
        model = JobPost
        fields = ['role',]
        widgets = {'skills': autocomplete.TaggitSelect2(url='autocomplete-skills')}


class UserUpdateForm(ModelForm):

    class Meta:
        model = User
        fields = ['first_name', 'email']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_method = 'post'
        self.fields['email'].label = 'Email'
        self.fields['first_name'].label = 'Full Name'
        self.helper.layout = Layout(

            Div(
                Div('email', css_class='col-sm-12'),
                Div('first_name', css_class='col-sm-12'),
                css_class='row'),
            Div(
                Submit('save', 'Save'),
                css_class='save-button')
            )

    def save(self, request):
        user = super(UserUpdateForm, self).save(request)

        if hasattr(user, 'applicant'):
            applicant = user.applicant
            applicant.full_name = user.get_full_name()
            applicant.save()
        return user


class CompanySubscribeForm(ModelForm):

    class Meta:
        model = Subscriber
        fields = ['email']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_method = 'post'
        self.fields['email'].label = False
        self.helper.layout = Layout(
            Div(
                Div(Field('email', placeholder='Email'), css_class='col-sm-8'),
                Div(Submit('submit', 'Subscribe', css_class="btn-action"), css_class='col-sm-4'),
                css_class='row')
            )


class PricingForm(Form):
    credits = IntegerField(required=True, min_value=1, initial=1)
    currency = ChoiceField(choices=Purchase.CURRENCY, required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['credits'].label = False
        self.fields['currency'].widget = HiddenInput()
        self.fields['credits'].widget.attrs['class'] = 'form-control credits-number'


class EditApplicationForm(ModelForm):

    class Meta:
        model = Application
        exclude = ['applicant', 'job_post', 'resume', 'created_date',]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['interview_date'].input_formats = ['%d.%m.%Y']
        self.helper = FormHelper(self)
        self.helper.form_method = 'POST'
        self.helper.form_id = 'edit_application_form'
        self.helper.layout = Layout(
            Div(
                Div('status', css_class='col-sm-6'),
                Div('applicant_rating', css_class='col-sm-6'),
                css_class='row'),
            Div('interview_date'),
            Div('comment')
            )
'''
    def clean_interview_date(self):
        date = self.cleaned_data.get('interview_date')
        interview_date = datetime.datetime.strptime(date, '%d.%m.%Y').date()
        print(interview_date)
        return interview_date

    def save(self, *args, **kwargs):
        self.clean_interview_date()
        return super().save(*args, **kwargs)

'''


class DirectApplicantForm(SignupForm, ModelForm):
    first_name = CharField(max_length=50)
    last_name = CharField(max_length=50)

    class Meta:
        model = Applicant
        fields = ['resume', 'role', 'experience',]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_method = 'POST'
        self.fields['email'].label = 'Email'
        self.fields['first_name'].label = 'First Name'
        self.fields['last_name'].label = 'Last Name'
        self.fields['resume'].label = 'CV'
        self.fields['resume'].help_text = 'Upload a CV to apply, it must be a PDF or Word Document.'
        self.fields['role'].label = 'Current Role'
        self.fields['role'].help_text = 'Select your current role.'
        self.fields['experience'].help_text = 'Years of experience in your current role.'

        self.helper.layout = Layout(
            Div(
                Div('email', css_class='col-sm-6'),
                Div('password1', css_class='col-sm-6'),
                css_class='row'),
            Div(
                Div('first_name', css_class='col-sm-6'),
                Div('last_name', css_class='col-sm-6'),
                css_class='row'),
            Div(
                Div('resume', css_class='col-sm-4'),
                Div('role', css_class='col-sm-4'),
                Div('experience', css_class='col-sm-4'),
                css_class='row'),
            Div(
                Submit('apply', 'Apply'),
                css_class='save-button'))

    def save(self, request):
        user = super(DirectApplicantForm, self).save(request)
        resume = self.cleaned_data.get('resume')

        applicant = Applicant(
            user=user,
            full_name=user.get_full_name(),
            resume=resume,
            role=self.cleaned_data['role'],
            experience=self.cleaned_data['experience'],
            location=self.cleaned_data['location'],
            resume_upload_date= datetime.date.today())

        applicant.save()

        return applicant


class LeadForm(ModelForm):
    class Meta:
        model = Lead
        exclude = ['date', 'email']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_method = 'POST'
        self.helper.form_action = 'earning-success'
        self.fields['role'].label = 'Current Role'

        self.helper.layout = Layout(
            Div(
                Div('role', css_class='col-sm-12'),
                Div('experience', css_class='col-sm-12'),
                css_class='row'),
            Div(
                Submit('submit', 'Submit', css_class='btn-action'),
                css_class='save-button'))


class ApplicantSearchForm(ModelForm):
    class Meta:
        model = Applicant
        fields = ['role', 'skills', 'experience', 'location']
        widgets = {'skills': autocomplete.TaggitSelect2(url='autocomplete-skills')}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_method = 'GET'
        self.fields['role'].required = False
        self.fields['skills'].required = False
        self.fields['experience'].required = False
        self.fields['location'].required = False
        self.fields['skills'].help_text='Search for the skills you are looking for in a candidate.'

        self.helper.layout = Layout(
            Div(
                Div('role', css_class='col-sm-4'),
                Div('experience', css_class='col-sm-4'),
                Div('location', css_class='col-sm-4'),
                css_class='row'),
                Div(Field('skills',css_class='js-example-basic-multiple'), css_class='col-sm-12'),
            Div(
                Submit('search', 'Search', css_class='btn-action'),
                css_class='save-button'))


class OfferForm(ModelForm):
    talent_id = CharField(max_length=50)

    class Meta:
        model = Offer
        fields = ['title', 'job_type', 'min_salary', 'max_salary', 'currency', 'role', 'location', 'message']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_method = 'POST'
        self.helper.form_id = 'offerForm'
        self.fields['talent_id'].widget = HiddenInput()
        self.fields['job_type'].label = 'Type of Job'
        self.fields['job_type'].help_text = 'Select the type of employment.'
        self.fields['min_salary'].label = 'Minimum Monthly Salary'
        self.fields['max_salary'].label = 'Maximum Monthly Salary'

        self.helper.layout = Layout(
            Div(
                Div('talent_id'),
                Div('title', css_class='col-sm-12'),
                Div(Field('role',css_class='js-example-basic-single', id="id_offer_role"), css_class='col-sm-12'),
                css_class="row"),
            Div(Div('job_type', css_class='col-sm-6'),
                Div('location', css_class='col-sm-6'),
                css_class='row'),
            Div(Div('min_salary', css_class='col-sm-6'),
                Div('max_salary', css_class='col-sm-6'),
                css_class='row'),
            Div(Div('currency', css_class='col-sm-12'),
                Div('message', css_class='col-sm-12'),
                css_class='row'),)

    def save(self, request, applicant):

        company = request.user.company
        offer, created = Offer.objects.get_or_create(
            applicant=applicant,
            company=company,
            defaults={
            'title': self.cleaned_data['title'],
            'role': self.cleaned_data['role'],
            'job_type':self.cleaned_data['job_type'],
            'location':self.cleaned_data['location'],
            'min_salary':self.cleaned_data['min_salary'],
            'max_salary':self.cleaned_data['max_salary'],
            'currency':self.cleaned_data['currency'],
            'message':self.cleaned_data['message']})
        offer.save()
        return offer

class SalarySearchForm(ModelForm):
    class Meta:
        model = Applicant
        fields = ['role', 'experience', 'location']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_method = 'GET'
        self.fields['role'].required = False
        self.fields['experience'].required = False
        self.fields['location'].required = False

        self.helper.layout = Layout(
            Div(
                Div('role', css_class='col-sm-4'),
                Div('experience', css_class='col-sm-3'),
                Div('location', css_class='col-sm-3'),
            Div(
                Submit('search', 'Search', css_class='btn-action'),
                css_class='save-button col-sm-2'),
            css_class='row'))
