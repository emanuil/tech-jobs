# Generated by Django 2.0 on 2018-02-09 17:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('jobboard', '0014_auto_20180127_2002'),
    ]

    operations = [
        migrations.CreateModel(
            name='Subscribers',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(max_length=254)),
                ('created_date', models.DateField(auto_now_add=True)),
            ],
        ),
        migrations.RenameField(
            model_name='application',
            old_name='application_date',
            new_name='created_date',
        ),
        migrations.RenameField(
            model_name='company',
            old_name='post_credits',
            new_name='credits',
        ),
        migrations.RenameField(
            model_name='purchases',
            old_name='purchase_amount',
            new_name='amount',
        ),
        migrations.RenameField(
            model_name='purchases',
            old_name='company_id',
            new_name='company',
        ),
        migrations.RenameField(
            model_name='purchases',
            old_name='post_credits',
            new_name='credits',
        ),
        migrations.RenameField(
            model_name='purchases',
            old_name='purchase_date',
            new_name='date',
        ),
        migrations.RenameField(
            model_name='searches',
            old_name='applicant_id',
            new_name='applicant',
        ),
        migrations.RenameField(
            model_name='searches',
            old_name='search_date',
            new_name='date',
        ),
        migrations.RemoveField(
            model_name='applicant',
            name='last_login_date',
        ),
        migrations.RemoveField(
            model_name='applicant',
            name='registration_date',
        ),
        migrations.RemoveField(
            model_name='application',
            name='application_status',
        ),
        migrations.RemoveField(
            model_name='company',
            name='featured_post_credits',
        ),
        migrations.RemoveField(
            model_name='company',
            name='last_login_date',
        ),
        migrations.RemoveField(
            model_name='company',
            name='registration_date',
        ),
        migrations.RemoveField(
            model_name='purchases',
            name='featured_credits',
        ),
        migrations.RemoveField(
            model_name='purchases',
            name='job_post_id',
        ),
        migrations.AddField(
            model_name='application',
            name='applicant_rating',
            field=models.CharField(blank=True, choices=[('1', 'Unqualified'), ('2', 'Poorly Qualified'), ('3', 'Qualified'), ('4', 'Well Qualified'), ('5', 'Highly Qualified')], max_length=1),
        ),
        migrations.AddField(
            model_name='application',
            name='comment',
            field=models.CharField(blank=True, max_length=500),
        ),
        migrations.AddField(
            model_name='application',
            name='interview_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='application',
            name='status',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AddField(
            model_name='company',
            name='address',
            field=models.CharField(blank=True, max_length=250),
        ),
        migrations.AddField(
            model_name='company',
            name='summary',
            field=models.CharField(blank=True, max_length=300),
        ),
        migrations.AddField(
            model_name='company',
            name='team_size',
            field=models.CharField(blank=True, choices=[('1-10', '1-10'), ('11-50', '11-50'), ('51-200', '51-200'), ('201-500', '201-500'), ('501-1000', '501-1000'), ('1001-5000', '1001-5000'), ('5000+', '5000+')], max_length=20),
        ),
        migrations.AlterField(
            model_name='jobpost',
            name='company',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='jobposts', to='jobboard.Company'),
        ),
        migrations.AddField(
            model_name='subscribers',
            name='company',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='jobboard.Company'),
        ),
    ]
