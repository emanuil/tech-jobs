# Generated by Django 2.0 on 2018-11-20 20:38

import ckeditor.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jobboard', '0034_auto_20181108_1128'),
    ]

    operations = [
        migrations.AlterField(
            model_name='applicant',
            name='phone',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AlterField(
            model_name='applicant',
            name='resume_upload_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='company',
            name='description',
            field=ckeditor.fields.RichTextField(),
        ),
    ]
