# Generated by Django 2.0 on 2018-01-07 13:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jobboard', '0003_auto_20180107_1302'),
    ]

    operations = [
        migrations.AlterField(
            model_name='jobpost',
            name='max_salary',
            field=models.PositiveIntegerField(blank=True),
        ),
        migrations.AlterField(
            model_name='jobpost',
            name='min_salary',
            field=models.PositiveIntegerField(blank=True),
        ),
    ]
