from rest_framework import serializers
from .models import Applicant, Company, JobPost, Application


class ApplicantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Applicant
        fields = ('user', 'phone', 'resume')
        depth = 1


class JobPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobPost
        exclude = ('created_date', 'boosted', 'expiry_date', 'expired')
        depth = 1


class CompanySerializer(serializers.ModelSerializer):
    jobposts = JobPostSerializer(many=True)

    class Meta:
        model = Company
        fields = ('name', 'website', 'jobposts')
        depth = 1


class ApplicationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Application
        fields = '__all__'
        depth = 1
