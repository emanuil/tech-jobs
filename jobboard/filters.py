import django_filters

from .models import JobPost, Company


class JobPostFilter(django_filters.FilterSet):
    title = django_filters.CharFilter(label='Title', lookup_expr='icontains')

    class Meta:
        model = JobPost
        fields = ['title']


class CompanyFilter(django_filters.FilterSet):

    class Meta:
        model = Company
        fields = {'name': ['icontains'], 'city': ['icontains']}
