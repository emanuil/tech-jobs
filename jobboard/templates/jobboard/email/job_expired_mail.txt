Hi {{company.user.first_name}},

Your job listing for {{job.title}} has expired. To repost the job, visit your dashboard at https://techhire.bg/{{company.slug}}/dashboard/

You received {{applicants}} applications for the role.

To review and manage all applications from our Applicant Tracking System visit the link below.
https://techhire.bg/{{link}}

Thanks,
Tech Hire
techhire.bg