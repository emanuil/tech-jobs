import datetime
import string
import base64

from django.db import models, transaction
from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField, JSONField
from django.utils.translation import gettext as _
from django.db.models import Q
from django.core.mail import send_mail, send_mass_mail, EmailMessage, EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.crypto import get_random_string
from django.db.models.signals import pre_save
from django.urls import reverse
from django.conf import settings
from django.core.validators import FileExtensionValidator

from taggit.managers import TaggableManager
from ckeditor.fields import RichTextField
# Create your models here.

DEV = (
    ('Software Developer', _('Software Developer')),
    ('Front-End Developer', _('Front-End Developer')),
    ('Back-End Developer', _('Back-End Developer')),
    ('Full-Stack Developer', _('Full-Stack Developer')),
    ('DevOps Developer', _('DevOps Developer')),
    ('Mobile Developer', _('Mobile Developer')),
    ('Database Developer', _('Database Developer')),
    ('QA Engineer', _('QA Engineer')),
    ('Team Lead', _('Team Lead')),
    ('CTO', 'CTO'),
    )

DESIGN = (
    ('Web Designer', _('Web Designer')),
    ('Graphic Designer', _('Graphic Designer')),
    ('UI/UX Designer', _('UI/UX Designer')),
    ('UX Researcher', _('UX Researcher')),
    )

SUPPORT = (
    ('Technical Support', _('Technical Support')),
    ('Customer Support', _('Customer Support')),
    ('Customer Service', _('Customer Service')),
    )

DATA = (
    ('Data Analyst', _('Data Analyst')),
    ('Data Scientist', _('Data Scientist')),
    )

PRODUCT = (
    ('Project Manager', _('Project Manager')),
    ('Product Manager', _('Product Manager')),
    ('Product Owner', _('Product Owner')),
    ('Scrum Master', _('Scrum Master')),
)

ADMIN = (
    ('System Administrator', _('System Administrator')),
    ('Database Administrator', _('Database Administrator')),
    ('Network Administrator', _('Network Administrator')),
    )

ENG = (
    ('Hardware Engineer', _('Hardware Engineer')),
    ('Network Engineer', _('Network Engineer')),
    )

MARKETING = (
    ('Content Marketer', _('Content Marketer')),
    ('Digital Marketer', _('Digital Marketer')),
    ('Social Media Manager', _('Social Media Manager')),
    ('Marketing Manager', _('Marketing Manager')),
    )

BUSINESS = (
    ('Business Analyst', _('Business Analyst')),
    ('Account Manager', _('Account Manager')),
    ('Sales Representative', _('Sales Representative')),
    ('Human Resources', _('Human Resources')),
    ('Consultant', _('Consultant')),
    ('Other', _('Other')),
    )

ROLES = DEV + DESIGN + SUPPORT + DATA + PRODUCT + ADMIN + ENG + MARKETING + BUSINESS

CURRENCIES = (
    ('BGN', 'BGN'),
    ('EUR', 'EUR'),
    ('USD', 'USD'),
    )

EXPERIENCE = (
    ('Intern', _('Intern')),
    ('Graduate', _('Graduate')),
    ('Junior', _('Junior')),
    ('Mid', _('Mid-level')),
    ('Senior', _('Senior')),
    ('Lead', _('Lead')),
    ('Manager', _('Manager')),
    )

JOB_TYPE = (
    ('Full-time', _('Full-time')),
    ('Contract', _('Contract')),
    ('Part-time', _('Part-time')),
    ('Internship', _('Internship')),
    ('Freelance', _('Freelance')),
    )

LOCATION = (
    ('Sofia', _('Sofia')),
    ('Plovdiv', _('Plovdiv')),
    ('Varna', _('Varna')),
    ('Burgas', _('Burgas')),
    ('Ruse', _('Ruse')),
    ('Stara Zagora', _('Stara Zagora')),
    ('Remote', _('Remote')),
    ('Work Abroad', _('Work Abroad')),
    ('Other', _('Other')),
    )

PUBLISHED = (
    ('Today', _('Today')),
    ('1 Day ago', _('1 Day ago')),
    ('3 Days ago', _('3 Days ago')),
    ('7 Days ago', _('7 Days ago')),
    ('15 Days ago', _('15 Days ago')),
    )


COMPANY_SIZE = (
    ('1-10', '1-10'),
    ('11-50', '11-50'),
    ('51-200', '51-200'),
    ('201-500', '201-500'),
    ('501-1000', '501-1000'),
    ('1001-5000', '1001-5000'),
    ('5000+', '5000+'),
    )

EDUCATION = (
    ('High School', _('High School')),
    ('Bachelors', _('Bachelors')),
    ("Master's", _('Masters')),
    ('PhD', _('PhD')),
    )

YEARS = (
    ('0-1', '0-1 Years'),
    ('1-3', '1-3 Years'),
    ('4-6', '4-6 Years'),
    ('7-10', '7-10 Years'),
    ('10+', '10+ Years'),
)


class Applicant(models.Model):
    ROLES = ROLES
    user       = models.OneToOneField(User, on_delete=models.CASCADE, related_name='applicant')
    full_name  = models.CharField(max_length=100)
    phone      = models.CharField(max_length=50, blank=True)
    location   = models.CharField(max_length=50, default='Sofia', choices=LOCATION)
    resume     = models.FileField(upload_to='', validators=[FileExtensionValidator(allowed_extensions=['pdf', 'doc', 'docx'])], blank=True)
    looking    = models.BooleanField(default=True)
    role       = models.CharField(max_length=50, choices=ROLES, null=True)
    roles      = ArrayField(models.CharField(max_length=50, choices=ROLES, blank=True), null=True, blank=True)
    degree     = models.CharField(max_length=50, blank=True, choices=EDUCATION)
    languages  = ArrayField(models.CharField(max_length=30, blank=True), null=True, blank=True)
    linkedin   = models.URLField(max_length=250, blank=True)
    github     = models.URLField(max_length=250, blank=True)
    behance    = models.URLField(max_length=250, blank=True)
    website    = models.URLField(max_length=250, blank=True)
    last_query = models.TextField(null=True, editable=False)
    experience = models.CharField(max_length=50, choices=YEARS)
    job_type   = models.CharField(max_length=50, choices=JOB_TYPE, blank=True)
    current_salary        = models.PositiveIntegerField(blank=True, null=True)
    desired_salary        = models.PositiveIntegerField(blank=True, null=True)
    marketing_consent     = models.BooleanField(default=False)
    last_query_date       = models.DateField(null=True, blank=True)
    last_application_date = models.DateField(null=True, blank=True)
    resume_upload_date    = models.DateField(null=True, blank=True)

    skills = TaggableManager(verbose_name='Skills')

    def __str__(self):
        return '{}'.format(self.full_name)

    def model_name(self):
        return self._meta.verbose_name

    def save(self, *args, **kwargs):
        created = self.pk is None
        super().save(*args, **kwargs)
        if created:
            self.onboarding_email()

    def onboarding_email(self):
        subject = 'Welcome to Tech Hire'
        link = 'applicant/{}/'.format(self.pk)
        context = {'name':self.full_name.split()[0], 'link': link}
        text_content = render_to_string('email/user_onboarding.txt', context)
        html_content = render_to_string('email/user_onboarding.html', context)
        mail = self.user.email
        msg = EmailMultiAlternatives(subject, text_content, 'mail@techhire.bg', [mail])
        msg.attach_alternative(html_content, 'text/html')
        msg.send()
        return


class Company(models.Model):
    user        = models.OneToOneField(User, on_delete=models.CASCADE, related_name='company')
    name        = models.CharField('Company Name', max_length=50, unique=True)
    slug        = models.SlugField(editable=False, unique=True, max_length=255)
    phone       = models.CharField('Company Phone', max_length=50)
    website     = models.URLField(max_length=250, blank=True)
    description = RichTextField()
    logo        = models.ImageField(null=True, blank=True)
    background  = models.ImageField(null=True, blank=True)
    city        = models.CharField(max_length=50, blank=True)
    address     = models.CharField(max_length=250, blank=True)
    team_size   = models.CharField(max_length=20, choices=COMPANY_SIZE, blank=True)
    benefits    = ArrayField(models.CharField(max_length=50), null=True, blank=True)
    credits     = models.DecimalField(max_digits=5, decimal_places=1, default=1, blank=True)
    recruiter   = models.BooleanField(default=False)
    marketing_consent     = models.BooleanField(default=True)
    contact_email         = models.EmailField(null=True, blank=True)
    notification_email    = models.EmailField(null=True, blank=True)
    recruiter_expiry_date = models.DateField(null=True, blank=True)
    last_job_post_date    = models.DateField(null=True, blank=True)
    modified_date         = models.DateField(auto_now=True)

    skills = TaggableManager(verbose_name='Skills', blank=True)

    def __str__(self):
        return '{}'.format(self.name)

    def get_absolute_url(self):
        return reverse('company-home', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        created = self.pk is None
        super().save(*args, **kwargs)
        if created:
            self.onboarding_email()

    def onboarding_email(self):
        subject = 'Welcome to Tech Hire'
        link = '{}/job/new/'.format(self.slug)
        context = {'company':self, 'link': link}
        text_content = render_to_string('email/company_onboarding.txt', context)
        html_content = render_to_string('email/company_onboarding.html', context)
        mail = self.user.email
        msg = EmailMultiAlternatives(subject, text_content, 'mail@techhire.bg', [mail])
        msg.attach_alternative(html_content, 'text/html')
        msg.send()
        return


class JobPostManager(models.Manager):
    def search(self, *args, **kwargs):
        qs = self.get_queryset()
        args = []
        params = {'expired': 'False'}
        if 'q' in kwargs:
            q = kwargs['q'][0]
            table = str.maketrans({key: None for key in string.punctuation})
            q = q.translate(table)
            args.append(Q(title__icontains=q) | Q(skills__name__in=q.split()))
        if 'company' in kwargs:
            params['company__name__icontains'] = kwargs['company'][0]
        if 'job_type' in kwargs:
            filters = None
            for job_type in kwargs['job_type'][0].split(','):
                query = Q(job_type__icontains=job_type)
                if filters:
                    filters |= query
                else:
                    filters = query
            args.append(filters)
        if 'roles' in kwargs:
            filters = None
            for role in kwargs['roles'][0].split(','):
                query = Q(role__icontains=role)
                if filters:
                    filters |= query
                else:
                    filters = query
            args.append(filters)
        if 'experience' in kwargs:
            filters = None
            for experience in kwargs['experience'][0].split(','):
                query = Q(experience__icontains=experience)
                if filters:
                    filters |= query
                else:
                    filters = query
            args.append(filters)
        if 'skills' in kwargs:
            params['skills__name__in'] = kwargs['skills']
        if 'min_salary' in kwargs:
            minimum = kwargs['min_salary'][0]
            args.append(Q(min_salary__gte=minimum)| Q(max_salary__gte=minimum))
        if 'max_salary' in kwargs:
            if kwargs['max_salary'][0] != '8000+':
                params['max_salary__lte'] = kwargs['max_salary'][0]
        if 'currency' in kwargs:
            params['currency'] = kwargs['currency'][0]
        if 'location' in kwargs:
            filters = None
            for loc in kwargs['location'][0].split(','):
                query = Q(location__icontains=loc)
                if filters:
                    filters |= query
                else:
                    filters = query
            args.append(filters)
        if 'published' in kwargs:
            value = kwargs['published'][0]
            if value == 'Today':
                value = 0
            else:
                value = value.split()[0]
            today = datetime.date.today()
            start_date = today - datetime.timedelta(days=int(value))
            params['publish_date__range'] = (start_date, today)

        return qs.filter(*args, **params).distinct().order_by('-featured', '-publish_date')


class JobPost(models.Model):
    ROLES = ROLES
    EXPERIENCE = EXPERIENCE
    JOB_TYPE = JOB_TYPE
    LOCATION = LOCATION
    PUBLISHED = PUBLISHED
    CURRENCIES = CURRENCIES

    title        = models.CharField(max_length=150)
    slug         = models.SlugField(editable=False, max_length=255)
    featured     = models.BooleanField(default=False)
    company      = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='jobposts')
    job_type     = models.CharField(max_length=50, choices=JOB_TYPE)
    min_salary   = models.PositiveIntegerField(blank=True, null=True)
    max_salary   = models.PositiveIntegerField(blank=True, null=True)
    currency     = models.CharField(max_length=3, choices=CURRENCIES, blank=True)
    role         = models.CharField(max_length=50, choices=ROLES)
    location     = models.CharField(max_length=50, choices=LOCATION, blank=True)
    experience   = models.CharField(max_length=50, choices=EXPERIENCE, blank=True)
    description  = RichTextField()
    created_date = models.DateField(auto_now_add=True, editable=False)
    publish_date = models.DateTimeField(null=True)
    expiry_date  = models.DateField(null=True)
    boosted      = models.BooleanField(default=False)
    expired      = models.BooleanField(default=True)

    objects = JobPostManager()

    skills = TaggableManager(verbose_name='Skills')

    def __str__(self):
        return '{} at {}'.format(self.title, self.company.name)

    def model_name(self):
        return self._meta.verbose_name

    def get_absolute_url(self):
        return reverse('job', kwargs={'id': self.pk, 'slug':self.slug})

    def save(self, *args, **kwargs):
        created = self.pk is None
        super().save(*args, **kwargs)
        if created and not self.expired:
            subscribers = Subscriber.objects.filter(company=self.company)
            subject = '{} posted a New Job!'.format(self.company)
            link = 'job/{}/{}/'.format(self.pk, self.slug)
            for sub in subscribers:
                context = {'job': self, 'link': link, 'pk': sub.pk}
                text_content = render_to_string('email/new_job_notification.txt', context)
                html_content = render_to_string('email/new_job_notification.html', context)
                msg = EmailMultiAlternatives(subject, text_content, 'mail@techhire.bg', [sub.email])
                msg.attach_alternative(html_content, 'text/html')
                msg.send()


class ApplicationManager(models.Manager):
    def search(self, *args, **kwargs):
        qs = self.get_queryset()
        args = []
        params = {}
        if 'q' in kwargs:
            q = kwargs['q'][0]
            table = str.maketrans({key: None for key in string.punctuation})
            q = q.translate(table)
            params['applicant__full_name__icontains'] = q
        if 'created_date' in kwargs:
            today = datetime.date.today()
            end_date = today - datetime.timedelta(days=int(kwargs['created_date'][0]))
            params['publish_date__range'] = (end_date, today)
        if 'status' in kwargs:
            filters = None
            for status in kwargs['status']:
                query = Q(status__icontains=status)
                if filters:
                    filters |= query
                else:
                    filters = query
            args.append(filters)
        if 'job_post' in kwargs:
            params['job_post'] = kwargs['job_post']
        if 'interview_date' in kwargs:
            today = datetime.date.today()
            end_date = today - datetime.timedelta(days=int(kwargs['interview_date'][0]))
            params['publish_date__range'] = (end_date, today)
        if 'applicant_rating' in kwargs:
            filters = None
            for rating in kwargs['applicant_rating']:
                query = Q(applicant_rating__icontains=rating)
                if filters:
                    filters |= query
                else:
                    filters = query
            args.append(filters)
        if 'skills' in kwargs:
            query = Q(applicant__skills__overlap=kwargs['skills'])
            args.append(query)

        return qs.filter(*args, **params).order_by('-created_date')


class Application(models.Model):
    CANDIDATE_RATING = (
        ('1', _('Unqualified')),
        ('2', _('Poorly Qualified')),
        ('3', _('Qualified')),
        ('4', _('Well Qualified')),
        ('5', _('Highly Qualified')),
        )
    STATUS = (
        ('Applied', 'Applied'),
        ('Interview', 'Interview'),
        ('Offer', 'Offer'),
        ('Hired', 'Hired'),
        ('Rejected', 'Rejected'),
        )

    applicant        = models.ForeignKey(Applicant, on_delete=models.CASCADE, related_name='applications')
    job_post         = models.ForeignKey(JobPost, on_delete=models.CASCADE, related_name='applications')
    resume           = models.FileField()
    created_date     = models.DateField(auto_now_add=True)
    status           = models.CharField(max_length=50, choices=STATUS, blank=True, default='Applied')
    interview_date   = models.DateField(null=True, blank=True)
    applicant_rating = models.CharField(max_length=1, choices=CANDIDATE_RATING, blank=True)
    comment          = models.CharField(max_length=500, blank=True)

    objects = ApplicationManager()

    def get_resume_url(self):
        return '<a href="self.resume.url" Download> Download CV</a>'

    def save(self, *args, **kwargs):
        created = self.pk is None
        super().save(*args, **kwargs)
        if created:
            self.email_application()
            self.confirm_application()

    def email_application(self):
        subject = 'New Application for {}'.format(self.job_post.title)
        link = '{}/job/{}/'.format(self.job_post.company.slug, self.job_post.pk)
        context = {'job': self.job_post, 'company':self.job_post.company, 'applicant': self.applicant, 'link': link, 'application': self}
        text_content = render_to_string('email/new_application.txt', context)
        html_content = render_to_string('email/new_application.html', context)
        if self.job_post.company.notification_email:
            mail = self.job_post.company.notification_email
        else:
            mail = self.job_post.company.user.email
        msg = EmailMultiAlternatives(subject, text_content, 'mail@techhire.bg', [mail])
        msg.attach_alternative(html_content, 'text/html')
        msg.send()
        return

    def confirm_application(self):
        applicant = self.applicant
        applicant.last_application_date = datetime.date.today()
        applicant.save()
        subject = 'Application Confirmation: {}'.format(self.job_post)
        context = {'job': self.job_post, 'name': self.applicant.user.first_name}
        text_content = render_to_string('email/application_confirmation.txt', context)
        html_content = render_to_string('email/application_confirmation.html', context)
        msg = EmailMultiAlternatives(subject, text_content, 'mail@techhire.bg', [applicant.user.email])
        msg.attach_alternative(html_content, 'text/html')
        msg.send()
        return

class Purchase(models.Model):
    CURRENCY = CURRENCIES

    company   = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='purchases')
    invoice   = models.CharField(max_length=20)
    amount    = models.CharField(max_length=50)
    currency  = models.CharField(max_length=3, choices=CURRENCIES, default='BGN')
    completed = models.BooleanField(default=False)
    credits   = models.PositiveIntegerField()
    date      = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.invoice

    def generate_order(self):
        exp = datetime.datetime.now() + datetime.timedelta(hours=4)
        expiring = datetime.datetime.strftime(exp, "%d.%m.%Y %H:%M")

        data = '''
            MIN={}
            INVOICE={}
            AMOUNT={}
            CURRENCY={}
            EXP_TIME={}
            '''.format(settings.MID, self.invoice, self.amount, self.currency, expiring)

        return base64.b64encode(data.encode())

    @transaction.atomic
    def payment_completed(self):
        self.completed = True
        company = self.company
        company.credits += self.credits
        self.save()
        company.save()
        subject = 'Payment Confirmation'
        link = '{}/dashboard/'.format(self.company.slug)
        context = {'company':self.company, 'link': link, 'purchase': self}
        text_content = render_to_string('email/payment_confirmation.txt', context)
        html_content = render_to_string('email/payment_confirmation.html', context)
        msg = EmailMultiAlternatives(subject, text_content, 'mail@techhire.bg', [company.user.email])
        msg.attach_alternative(html_content,'text/html')
        msg.send()
        return

def unique_invoice_generator(instance):
    invoice_id = get_random_string(length=10, allowed_chars='1234567890')
    Klass = instance.__class__
    qs_exists = Klass.objects.filter(invoice=invoice_id).exists()
    if qs_exists:
        return unique_invoice_generator(instance)
    return invoice_id

def pre_save_create_invoice(sender, instance, *args, **kwargs):
    if not instance.invoice:
        instance.invoice = unique_invoice_generator(instance)

pre_save.connect(pre_save_create_invoice, sender=Purchase)


class Subscriber(models.Model):
    company      = models.ForeignKey(Company, on_delete=models.CASCADE)
    email        = models.EmailField()
    created_date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.company.name + str(self.id)


class Salary(models.Model):
    role            = models.CharField(max_length=50, choices=ROLES)
    experience      = models.CharField(max_length=50, choices=YEARS)
    location        = models.CharField(max_length=50, choices=LOCATION)
    salary          = models.PositiveIntegerField()
    date            = models.DateField(auto_now=True)


class Lead(models.Model):
    email      = models.CharField(max_length=150)
    role       = models.CharField(max_length=50, choices=ROLES)
    experience = models.CharField(max_length=50, choices=YEARS)
    date       = models.DateField(auto_now=True)


class Offer(models.Model):
    applicant    = models.ForeignKey(Applicant, on_delete=models.CASCADE, related_name='offers')
    company      = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='offers')
    title        = models.CharField(max_length=150)
    role         = models.CharField(max_length=50, choices=ROLES)
    job_type     = models.CharField(max_length=50, choices=JOB_TYPE)
    location     = models.CharField(max_length=50, choices=LOCATION, default='Sofia')
    min_salary   = models.PositiveIntegerField()
    max_salary   = models.PositiveIntegerField()
    currency     = models.CharField(max_length=3, choices=CURRENCIES, default='BGN')
    message      = models.TextField(blank=True)
    active       = models.BooleanField(default=True)
    interested   = models.BooleanField(default=False)
    interview    = models.BooleanField(default=False)
    final_offer  = models.BooleanField(default=False)
    accepted     = models.BooleanField(default=False)
    created_date   = models.DateTimeField(auto_now_add=True)
    interview_date = models.DateTimeField(null=True, blank=True)
    final_offer_date = models.DateField(null=True, blank=True)
    accepted_date = models.DateField(null=True, blank=True)


class Message(models.Model):
    offer = models.ForeignKey(Offer, on_delete=models.CASCADE, related_name='messages')
    author = models.CharField(max_length=150)
    message = models.TextField()
    attachment = models.FileField(upload_to='', null=True)
    timestamp = models.DateTimeField(auto_now_add=True)
