from django.shortcuts import render
from .models import BlogPost
from jobboard.models import JobPost
# Create your views here.

def blog(request, year, month, day, slug):
    blog = BlogPost.objects.get(slug=slug)
    context = {'blog': blog, 'description': blog.intro, 'page_title': blog.title}
    if blog.query:
        jobs = JobPost.objects.search(**blog.query).filter(featured=True)[:3]
        context.update({'job_1': jobs[:1], 'job_2':jobs[1:2]})
    return render(request, 'blog/blog_post.html', context)


def blog_list(request):
    blogs = BlogPost.objects.all().order_by('-timestamp')
    return render(request, 'blog/blog_list.html', {'post_list': blogs})