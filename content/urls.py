from django.urls import path

from . import views

urlpatterns = [
    path('', views.blog_list, name='blog'),
    path('<int:year>/<int:month>/<int:day>/<slug:slug>/', views.blog, name='blog-post'),
    ]