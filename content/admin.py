from django.contrib import admin
from .models import BlogPost
# Register your models here.

class BlogPostAdmin(admin.ModelAdmin):
    '''
        Admin View for Content
    '''
    list_display = ('title', 'slug', 'author', 'query', 'timestamp')
    list_filter = ('author',)
    search_fields = ('title',)
    prepopulated_fields = {"slug": ("title",)}


admin.site.register(BlogPost, BlogPostAdmin)