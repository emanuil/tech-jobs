from django.db import models
from ckeditor.fields import RichTextField
from django.contrib.postgres.fields import JSONField
from django.urls import reverse

# Create your models here.


class BlogPost(models.Model):
    title = models.CharField(max_length=200)
    slug  = models.SlugField(max_length=255, unique=True)
    author = models.CharField(max_length=50)
    intro = RichTextField(config_name='admin')
    content = RichTextField(config_name='admin')
    query = JSONField(null=True, blank=True)
    timestamp = models.DateField()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        kwargs = {
        'year': self.timestamp.strftime('%Y'),
        'month': self.timestamp.strftime('%m'),
        "day": self.timestamp.strftime("%d"),
        "slug": self.slug,
        }
        return reverse('blog:blog-post', kwargs=kwargs)