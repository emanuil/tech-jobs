var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var minifycss = require('gulp-minify-css');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var gzip = require('gulp-gzip');
var concat = require('gulp-concat');
var babel = require('gulp-babel');
var spawn = require('child_process').spawn;
var bs = require('browser-sync').create();


var paths = {
    styles: {
        src: 'jobboard/static/jobboard/css/main.scss',
        dest: 'jobboard/static/jobboard/css'
    },
    scripts: {
        src: 'jobboard/static/jobboard/js/script/*.js',
        dest: 'jobboard/static/jobboard/js'
    }
};


var gzip_options = {
    threshold: '1kb',
    gzipOptions: {
        level: 9
    }
};

function styles() {
    return gulp.src(paths.styles.src)
        .pipe(sass().on('error', sass.logError))
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
        .pipe(gulp.dest(paths.styles.dest));
}

function css() {
    return gulp.src('jobboard/static/jobboard/css/landing.css')
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
        .pipe(gulp.dest(paths.styles.dest));
}


function scripts() {
  return gulp.src(paths.scripts.src)
    .pipe(babel({
        presets: ['es2015']
    }))
    .pipe(uglify())
    .pipe(rename({suffix:'.min'}))
    .pipe(gulp.dest(paths.scripts.dest));
}


function server() {
    var server = spawn('python', ['manage.py', 'runserver'], {stdio: 'inherit'});
}

exports.server = server;
exports.styles = styles;
exports.scripts = scripts;

var build = gulp.series(gulp.parallel(styles, scripts));


gulp.task('browserSync', function() {
    bs.init(
        {proxy: 'localhost:8000'});
});

gulp.task('default', gulp.parallel(build, server, 'browserSync', function() {
    gulp.watch('./jobboard/templates/jobboard/*.html').on('change', bs.reload);
    gulp.watch('./jobboard/templates/jobboard/account/*.html').on('change', bs.reload);
    gulp.watch('./jobboard/*.py').on('change', bs.reload);
    gulp.watch('./jobboard/static/jobboard/css/landing.css', css).on('change', bs.reload);
    gulp.watch(paths.styles.src, styles).on('change', bs.reload);
    gulp.watch(paths.scripts.src, scripts).on('change', bs.reload);

}));

