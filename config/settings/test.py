from .base import *

SECRET_KEY = env('DJANGO_SECRET_KEY', default='^cjc-7(q+6u75d+n4@shiw+y8gv&f+ft$5)i6o)g72td#wd6cv')

ALLOWED_HOSTS = ['techhire.bg', 'techhire-dev.eu-west-1.elasticbeanstalk.com', 'www.techhire.bg', 'localhost']

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ['RDS_DB_NAME'],
        'USER': os.environ['RDS_USERNAME'],
        'PASSWORD': os.environ['RDS_PASSWORD'],
        'HOST': os.environ['RDS_HOSTNAME'],
        'PORT': os.environ['RDS_PORT'],
    }
}
AWS_ACCESS_KEY_ID = os.environ['AWS_ACCESS_KEY_ID']
AWS_SECRET_ACCESS_KEY = os.environ['AWS_SECRET_ACCESS_KEY']
##########################################################################
# Celery Settings
##########################################################################
CELERY_DEFAULT_QUEUE = os.environ['CELERY_DEFAULT_QUEUE']
CELERY_QUEUES = {
    CELERY_DEFAULT_QUEUE: {
        'exchange': CELERY_DEFAULT_QUEUE,
        'binding_key': CELERY_DEFAULT_QUEUE,
    }
}

CELERY_BEAT_SCHEDULER = 'django_celery_beat.schedulers:DatabaseScheduler'
CELERY_RESULT_BACKEND = 'django-db'
CELERY_TASK_SERIALIZER ='json'

CELERY_TASK_IGNORE_RESULT = True
CELERY_TASK_STORE_ERRORS_EVEN_IF_IGNORED = True
CELERY_BROKER_URL = 'sqs://{}:{}@'.format(os.environ['AWS_ACCESS_KEY_ID'],os.environ['AWS_SECRET_ACCESS_KEY'])
BROKER_TRANSPORT_OPTIONS = {'region': 'eu-west-1'}

##########################################################################
# Email Settings
##########################################################################
DEFAULT_FROM_EMAIL = 'mail@techhire.bg'
EMAIL_BACKEND = 'django_ses.SESBackend'
AWS_SES_REGION_NAME = 'eu-west-1'
AWS_SES_REGION_ENDPOINT = 'email.eu-west-1.amazonaws.com'

##########################################################################
# STORAGE CONFIGURATION
##########################################################################

AWS_ACCESS_KEY_ID = os.environ['AWS_ACCESS_KEY_ID']
AWS_SECRET_ACCESS_KEY = os.environ['AWS_SECRET_ACCESS_KEY']
AWS_STORAGE_BUCKET_NAME = os.environ['AWS_STORAGE_BUCKET_NAME']
AWS_FILES_BUCKET_NAME = os.environ['AWS_FILES_BUCKET_NAME']
AWS_S3_REGION_NAME = 'eu-west-1'
AWS_S3_CUSTOM_DOMAIN = 'd12bkdlz89m1j5.cloudfront.net'
AWS_S3_FILE_OVERWRITE = False

AWS_QUERYSTRING_AUTH = False
AWS_HEADERS = {
    'Expires': 'Mon, 3 Jan 2022 20:00:00 GMT',
    'CacheControl': 'max-age=4608000'
}
AWS_IS_GZIPPED = True

DEFAULT_FILE_STORAGE = 'jobboard.handlers.CustomBoto3Storage'
STATICFILES_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'

STATIC_URL = 'https://' + AWS_S3_CUSTOM_DOMAIN + '/'

STATIC_ROOT = str(ROOT_DIR('staticfiles'))

STATICFILES_DIRS = (str(APPS_DIR.path('static')),)

MEDIA_URL = '/content/'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

##########################################################################
# Payment Settings
##########################################################################
EPAY_SECRET = os.environ['EPAY_SECRET']

MID = os.environ['MID']
