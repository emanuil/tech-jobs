from .base import *


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env('DJANGO_SECRET_KEY', default='^cjc-7(q+6u75d+n4@shiw+y8gv&f+ft$5)i6o)g72td#wd6cv')

DEBUG = env.bool('DJANG0_DEBUG', default=True)

INTERNAL_IPS = ('127.0.0.1', 'localhost')

ALLOWED_HOSTS = ['408124b5.ngrok.io', 'localhost']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'tech_hire',
        'USER': 'tech_hire',
        'PASSWORD': '05112017@Uk',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}


EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

CRISPY_FAIL_SILENTLY = not DEBUG


##########################################################################
# STORAGE CONFIGURATION
##########################################################################

STATIC_URL = '/static/'

STATIC_ROOT = str(ROOT_DIR('staticfiles'))

STATICFILES_DIRS = (
    str(APPS_DIR.path('static')),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

MEDIA_ROOT = str(APPS_DIR.path('media'))

MEDIA_URL = '/media/'


##########################################################################
# Payment Settings
##########################################################################
EPAY_SECRET = env('EPAY_SECRET')

MID = env('MID')

##########################################################################
# CKeditor Settings
##########################################################################

CKEDITOR_BASEPATH = '/static/ckeditor/ckeditor/'

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'Custom',
        'toolbar_Custom': [
            ['Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList'],
            ['Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['Link', 'Unlink'],
        ],
        'width': '100%',
    },
    'admin':{
        'toolbar': 'Custom',
    }
}
