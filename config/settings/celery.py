from .base import *
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

SECRET_KEY = env('DJANGO_SECRET_KEY')

DEBUG = False

ALLOWED_HOSTS = ['techhire.bg', 'www.techhire.bg', 'techhire.eu-west-1.elasticbeanstalk.com', 'techhire-prod-1.eu-west-1.elasticbeanstalk.com']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': env('RDS_DB_NAME'),
        'USER': env('RDS_USERNAME'),
        'PASSWORD': env('RDS_PASSWORD'),
        'HOST': env('RDS_HOSTNAME'),
        'PORT': env('RDS_PORT'),
    }
}

##########################################################################
# Celery Settings
##########################################################################
CELERY_DEFAULT_QUEUE = env('CELERY_DEFAULT_QUEUE')

CELERY_BEAT_SCHEDULER = 'django_celery_beat.schedulers:DatabaseScheduler'
#CELERY_RESULT_BACKEND = 'db+postgresql://{}:{}@{}:{}/{}'.format(os.environ['RDS_USERNAME'], os.environ['RDS_PASSWORD'], os.environ['RDS_HOSTNAME'],$
CELERY_TASK_SERIALIZER ='json'

CELERY_TASK_IGNORE_RESULT = True
CELERY_TASK_STORE_ERRORS_EVEN_IF_IGNORED = True

CELERY_BROKER_URL = 'sqs://{}:{}@'.format(env('AWS_ACCESS_KEY_ID'), env('AWS_SECRET_ACCESS_KEY'))
BROKER_TRANSPORT_OPTIONS = {'region': 'eu-west-1'}

##########################################################################
# Email Settings
##########################################################################
DEFAULT_FROM_EMAIL = 'mail@techhire.bg'
EMAIL_BACKEND = 'django_ses.SESBackend'
AWS_SES_REGION_NAME = 'eu-west-1'
AWS_SES_REGION_ENDPOINT = 'email.eu-west-1.amazonaws.com'

##########################################################################
# STORAGE CONFIGURATION
##########################################################################

AWS_ACCESS_KEY_ID = env('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = env('AWS_SECRET_ACCESS_KEY')
AWS_STORAGE_BUCKET_NAME = env('AWS_STORAGE_BUCKET_NAME')
AWS_FILES_BUCKET_NAME = env('AWS_FILES_BUCKET_NAME')
AWS_S3_REGION_NAME = 'eu-west-1'
AWS_S3_CUSTOM_DOMAIN = 'd2gbvrqt4one8o.cloudfront.net'
AWS_S3_FILE_OVERWRITE = False

AWS_QUERYSTRING_AUTH = False

AWS_S3_OBJECT_PARAMETERS = {
    'CacheControl': 'max-age=2628000',
}

AWS_IS_GZIPPED = True

DEFAULT_FILE_STORAGE = 'jobboard.handlers.CustomBoto3Storage'
STATICFILES_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'

STATIC_URL = 'https://' + AWS_S3_CUSTOM_DOMAIN + '/'

STATIC_ROOT = str(ROOT_DIR('staticfiles'))

STATICFILES_DIRS = (str(APPS_DIR.path('static')),)

MEDIA_URL = '/media/'

ADMIN_MEDIA_PREFIX = STATIC_URL + 'admin/'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

##########################################################################
# Payment Settings
##########################################################################
EPAY_SECRET = env('EPAY_SECRET')

MID = env('MID')

##########################################################################
# Sentry Setup
##########################################################################
sentry_url = env('SENTRY_URL')

sentry_sdk.init(dsn=sentry_url, integrations=[DjangoIntegration()])

##########################################################################
# CKeditor Settings
##########################################################################

CKEDITOR_BASEPATH = STATIC_URL + 'ckeditor/ckeditor/'

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'Custom',
        'toolbar_Custom': [
            ['Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList'],
            ['Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['Link', 'Unlink'],
        ],
        'width': '100%',
    }
}